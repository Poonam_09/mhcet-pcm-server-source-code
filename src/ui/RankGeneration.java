package ui;

import com.Model.TitleInfo;
import com.bean.PracticeBean;
import com.bean.QuestionBean;
import com.bean.RankBean;
import com.bean.SendSMSBean;
import com.bean.StudentBean;
import com.bean.TestBean;
import com.bean.UnitTestBean;
import com.db.operations.SaveTestBlobOperation;
import com.db.operations.ClassSaveTestOperation;
import com.db.operations.RegistrationOperation;
import com.db.operations.StudentRegistrationOperation;
import com.db.operations.SubjectOperation;
import com.db.operations.TotalMarkOperation;
import java.awt.Color;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.lowagie.text.Chunk;
import java.util.LinkedHashMap;
import javax.swing.ImageIcon;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.pages.HomePage;
import com.pages.SendTestResult;
import de.nixosoft.jlr.JLROpener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;

public class RankGeneration extends javax.swing.JFrame {

    ArrayList<Integer> testId = new ArrayList<Integer>();
    ArrayList<QuestionBean> testQuestions = new ArrayList<QuestionBean>();
    ArrayList<Double> pmarks = new ArrayList<Double>();
    ArrayList<Double> nmarks = new ArrayList<Double>();
    ArrayList<Integer> totalQue = new ArrayList<Integer>();
    ArrayList<String> testInfo = new ArrayList<String>();
    ArrayList<String> subNames = new ArrayList<String>();
    ArrayList<TestBean> testBean1;
    LinkedHashMap<Integer, Double> lh;
    ArrayList<RankBean> rankBeanList = new ArrayList<RankBean>();
    ArrayList<PracticeBean> practiceBean1;
    ArrayList<UnitTestBean> unitTestBean1;
    ArrayList<Double> yourArray = new ArrayList<Double>();
    ArrayList<Integer> testIds1;
    java.awt.event.MouseEvent evt111;
    int subjectId, chapterId, i = 0, Idoftest;
    Calendar cal = Calendar.getInstance();
    Calendar cal1 = Calendar.getInstance();
    boolean flag = true;
    int subId;
    int testId1;
    double singlepmark, singlenmark;
    UnitTestBean ub=null;
    StudentBean studentBean=null;
    ArrayList<StudentBean> studentBeanList = new ArrayList<StudentBean>();
    SendSMSBean sendSMSBean=null;
//   ArrayList<Double> singlepmark=new ArrayList<Double>();
//   ArrayList<Double> singlenmark=new ArrayList<Double>();;
    

    /**
     * Creates new form TestResultForm
     */
    public void tablesetting() {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblGenerateRanking.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tblGenerateRanking.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tblGenerateRanking.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tblGenerateRanking.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
    }

    private void clearTableRows(javax.swing.JTable tblGenerateRanking) {
        DefaultTableModel model = (DefaultTableModel) tblGenerateRanking.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
//thi function is in use

    private void resumeTest(double singlepmark, double  singlenmark) {
        new TotalMarkOperation().clearTotalMark();
        
        for (int i = 0; i <rankBeanList.size(); i++) {
            RankBean rr = rankBeanList.get(i);
            int currentroll = rr.getRollNo();
            int testBeanId = rr.getTestbeanId();
            ArrayList<QuestionBean> qb = new ArrayList<QuestionBean>();
            qb = new SaveTestBlobOperation().getTestDetails(currentroll, testBeanId);
            int c = 0, w = 0, una = 0;
            double total=0,wrongTotal=0;
            System.out.println("qb.size()="+qb.size());
            for (int j = 0; j <qb.size(); j++) {
                int indx=0;
                indx=qb.get(j).getSubjectId();
                System.out.println("***indx="+indx);
                
//                 singlepmark= pmarks.get(indx-1);               
//                 singlenmark= nmarks.get(indx-1);   
                singlepmark= pmarks.get(0);               
                singlenmark= nmarks.get(0); 
                
                if (qb.get(j).getUserAnswer().equals(qb.get(j).getAnswer())) {
                    c++;
                    total = total+singlepmark;
                } else if (qb.get(j).getUserAnswer().equals("UnAttempted")) {
                    una++;
                }
                else{
                    wrongTotal = wrongTotal+singlenmark;
                }
            }
            una = c + una;
            w = qb.size() - una;
            double Ftotal = total - wrongTotal;
            
       // TotalMark insert into index of rb
            
            new TotalMarkOperation().insertIntoTotalMark(rr.getRollNo(), Ftotal,rr.getTimeinstring(),i);
            Ftotal = new RestrctDoubleUpto2().round(Ftotal, 2);
            System.out.println(rr.getRollNo() + " --> " + c + ", " + w + " --> " + Ftotal);
        }
        String subs = "";
        for (int sub = 0; sub < subNames.size(); sub++) {
            if (subs.equals("")) {
                subs += subNames.get(sub);
            } else {
                subs += ", " + subNames.get(sub);
            }
        }
        lblSub.setText("Subject: " + subs);
        ArrayList<Double> totalMarks = new ArrayList<Double>();
        totalMarks = new TotalMarkOperation().getTotalMarks();
        ArrayList<Integer> allroll = new ArrayList<Integer>();
        ArrayList<String> allrollDate = new ArrayList<String>();        
        allroll = new TotalMarkOperation().getTotalRolls();
        allrollDate = new TotalMarkOperation().getTotalRollsDate();
        DefaultTableModel model = (DefaultTableModel) tblGenerateRanking.getModel();
        clearTableRows(tblGenerateRanking);
        for (int i = 0; i < rankBeanList.size(); i++) {
                                   
//            int RollNo=allroll.get(i);            
//            System.out.println("RollNo"+RollNo);
                       
//            double ObtainMark=new ClassSaveTestOperation().getObtainMark(rb.get(i).getUnitTestId(), rb.get(i).getRollNo()); 
            String StudentName=new StudentRegistrationOperation().getNameOfStudent1(rankBeanList.get(i).getRollNo());
            System.out.println("StudentName*************"+StudentName);
            double Ftotal = new RestrctDoubleUpto2().round(totalMarks.get(i), 2);
            model.addRow(new Object[]{i + 1, rankBeanList.get(i).getRollNo(),StudentName, rankBeanList.get(i).getTotal_Questions(),rankBeanList.get(i).getObtainMark(), rankBeanList.get(i).getTimeinstring()});
            
//            studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankBeanList.get(i).getRollNo());
//            System.out.println(""+studentBeanList.get(i).getStudentName());
//            studentBean=new StudentRegistrationOperation().getAllInfoStudent1(rankBeanList.get(i).getRollNo());
        }
    }

    private void resumeTest() {
        DefaultTableModel model = (DefaultTableModel) tblGenerateRanking.getModel();
        clearTableRows(tblGenerateRanking);
        for (int i = 0; i < rankBeanList.size(); i++) {
            String temp = testInfo.get(i);
            String[] obj = temp.split("\t");
            int testId = Integer.parseInt(obj[0]);
//            String subjectName = con.getSubjectName(testInfo.get(1).g);
//            if(subjectName.equals("")){
//                subjectName = "All";
//            }
            int count = new ClassSaveTestOperation().getQuestionCount(testId);
            String date = obj[2];
            String status = obj[1];
            model.addRow(new Object[]{testId, count, date, status});
        }
    }
//UTest_Result_Info(unitTestId INTEGER,rollNo INTEGER,subject_Id INTEGER,total_Questions INTEGER,correct_Questions DOUBLE,PRIMARY KEY(unitTestId,subject_Id)

    public RankGeneration() {
        initComponents();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        String titl = new TitleInfo().getTitle();
        getContentPane().setBackground(Color.white);
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
    }

    public RankGeneration(ArrayList<RankBean> rankBeanList, int testId) {
        initComponents();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        this.rankBeanList = rankBeanList;
        getContentPane().setBackground(Color.white);
        this.Idoftest = testId;
        this.testId1=testId;
        ArrayList<Integer> subids = new ArrayList<Integer>();
        
        subids = new ClassSaveTestOperation().getsubjectIds(testId);
        
        
       if (subids.size() > 0) {
            pmarks = new SubjectOperation().getPMarks(subids);
            nmarks = new SubjectOperation().getNMarks(subids);
            subNames = new SubjectOperation().getAllSubjectName(subids);
            singlepmark = new SubjectOperation().getPerQuestionMarks(subids.get(i));
            singlenmark = new SubjectOperation().getSPerwrongQuestionMarks(subids.get(i));
            System.out.println("pmarks" +pmarks.get(0));
            System.out.println("nmarks" +nmarks.get(0));
            System.out.println("singlepmark" +singlepmark);
            System.out.println("singlenmark" +singlenmark);
            resumeTest(singlepmark, singlenmark);
       }
        
       
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lblSub = new javax.swing.JLabel();
        btnSumm = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        panePhysics = new javax.swing.JScrollPane();
        tblGenerateRanking = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        }
        ;
        LblHeader = new javax.swing.JLabel();
        BtnSendSMS = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Scholars Katta's NEET+JEE Software 2014");
        setBackground(new java.awt.Color(196, 223, 254));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setName("jPanel2"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(29, 9, 44));
        jPanel1.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel3.setBackground(new java.awt.Color(29, 9, 44));
        jPanel3.setName("jPanel3"); // NOI18N

        lblSub.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblSub.setForeground(new java.awt.Color(255, 255, 255));
        lblSub.setText("jLabel1");
        lblSub.setName("lblSub"); // NOI18N

        btnSumm.setBackground(new java.awt.Color(255, 255, 255));
        btnSumm.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSumm.setForeground(new java.awt.Color(29, 9, 44));
        btnSumm.setText("Print Summary");
        btnSumm.setName("btnSumm"); // NOI18N
        btnSumm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSummActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnBack.setText("Back");
        btnBack.setName("btnBack"); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        panePhysics.setBackground(new java.awt.Color(29, 9, 44));
        panePhysics.setForeground(new java.awt.Color(255, 255, 255));
        panePhysics.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        panePhysics.setName("panePhysics"); // NOI18N

        ((DefaultTableCellRenderer)tblGenerateRanking.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblGenerateRanking.setAutoCreateRowSorter(true);
        tblGenerateRanking.setBackground(new java.awt.Color(29, 9, 44));
        tblGenerateRanking.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        tblGenerateRanking.setForeground(new java.awt.Color(255, 255, 255));
        tblGenerateRanking.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Rank", "User Id", "Student Name", "Total Questions", "Obtained Marks", "Test Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblGenerateRanking.setToolTipText("Double Click To View Detail Result.");
        tblGenerateRanking.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblGenerateRanking.setGridColor(new java.awt.Color(0, 0, 0));
        tblGenerateRanking.setName("tblGenerateRanking"); // NOI18N
        tblGenerateRanking.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblGenerateRanking.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblGenerateRanking.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblGenerateRankingMouseClicked(evt);
            }
        });
        panePhysics.setViewportView(tblGenerateRanking);

        LblHeader.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 20)); // NOI18N
        LblHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeader.setText("Result Information");
        LblHeader.setName("LblHeader"); // NOI18N

        BtnSendSMS.setBackground(new java.awt.Color(255, 255, 255));
        BtnSendSMS.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        BtnSendSMS.setText("Send SMS");
        BtnSendSMS.setName("BtnSendSMS"); // NOI18N
        BtnSendSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSendSMSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSub)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(LblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnSendSMS, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSumm)
                .addContainerGap())
            .addComponent(panePhysics, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSub)
                    .addComponent(btnSumm)
                    .addComponent(btnBack)
                    .addComponent(LblHeader)
                    .addComponent(BtnSendSMS))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panePhysics, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tblGenerateRankingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGenerateRankingMouseClicked
    if (evt.getClickCount() == 2) {
        int row = tblGenerateRanking.rowAtPoint(evt.getPoint());
        if (row >= 0) {
            int roll = Integer.parseInt(tblGenerateRanking.getModel().getValueAt(row, 1).toString());
            String ttt = tblGenerateRanking.getModel().getValueAt(row, 5).toString();
            System.out.println("Swapna: " + roll);
            for (int ii = 0; ii < rankBeanList.size(); ii++) {
                if (roll == rankBeanList.get(ii).getRollNo()&& rankBeanList.get(ii).getTimeinstring().equals(ttt)) {
                    RankBean rankBean = new RankBean();
                    rankBean = rankBeanList.get(ii);
                    int testBeanId = rankBean.getTestbeanId();
                    System.out.println("testBeanId="+testBeanId);
                    int rollNo = rankBean.getRollNo();
                    ArrayList<QuestionBean> qb = new ArrayList<QuestionBean>();
                    qb =new SaveTestBlobOperation().getTestDetails(rollNo, testBeanId);
                    ub = new UnitTestBean();
                    ub.setQuestions(qb);
                    ub.setUnitTestId(testBeanId);
                    ub.setTotalTime(rollNo);
//                    for (int i = 0; i < qb.size(); i++) {
//                        System.out.println(qb.get(i).getQuestion_Id() + ":" + qb.get(i).getUserAnswer() + "-->" + qb.get(i).getAnswer());
//                    }
                   ViewStudentTestDetails vstd = new ViewStudentTestDetails(ub, false, false, testBeanId, this,rankBean);
                    vstd.setVisible(true);
                    this.dispose();
                }
            }
//            JOptionPane.showMessageDialog(null, rollNo + " : " + testBeanId + " : " + qb.size());
        }
    }
}//GEN-LAST:event_tblGenerateRankingMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do you want to close this task?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void btnSummActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSummActionPerformed
        //    print();
        int koko = 0;
        try {
//            s.execute("CREATE TABLE  TotalMark(rollNo INTEGER NOT NULL,Total double NOT NULL)");
            String directoryTosave = "";
            String fileName = "";
            try {
                JFileChooser saveFile = new JFileChooser();//new save dialog  
                saveFile.resetChoosableFileFilters();
                saveFile.setFileFilter(new FileFilter()//adds new filter into list  
                {
                    String description = "PDF Files(*.pdf)";//the filter you see  
                    String extension = "pdf";//the filter passed to program  

                    public String getDescription() {
                        return description;
                    }

                    public boolean accept(File f) {
                        if (f == null) {
                            return false;
                        }
                        if (f.isDirectory()) {
                            return true;
                        }
                        return f.getName().toLowerCase().endsWith(extension);
                    }
                });
                saveFile.setCurrentDirectory(new File("*.pdf"));
                int result = saveFile.showSaveDialog(this);
                // strSaveFileMenuItem is the parent Component which calls this method, ex: a toolbar, button or a menu item.  
                fileName = saveFile.getSelectedFile().getName();
                directoryTosave = saveFile.getSelectedFile().getPath();
                directoryTosave = saveFile.getCurrentDirectory().getPath();
                System.out.println(fileName + "     path : " + directoryTosave);
                koko = 1;
                // the file name selected by the user is now in the string 'strFilename'.  
            } catch (Exception er) {
             //      statusBar.setText("Error Saving File:" + er.getMessage()+"\n");  
            }
            if (koko == 1) {
                Document document = new Document();
      //        String t = JOptionPane.showInputDialog("Enter Name of File.");
                String fna = directoryTosave + "/" + fileName + ".pdf";
                PdfWriter.getInstance(document, new FileOutputStream(fna));
                document.open();

                //for heading......
                Font font = FontFactory.getFont("Times-Roman", 14, Font.BOLD);
                Font font1 = FontFactory.getFont("Times-Roman", 12, Font.NORMAL);
                Font font3 = FontFactory.getFont("Times-Roman", 8, Font.BOLD);
                Font font4 = FontFactory.getFont("Times-Roman", 8, Font.NORMAL);
                
                Paragraph paragraph = new Paragraph("", font);
                Paragraph paragraph1 = new Paragraph("Test Result Details", font1);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                paragraph1.setAlignment(Element.ALIGN_CENTER);
                paragraph.setSpacingAfter(3);
                paragraph1.setSpacingAfter(15);
                String instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
                Chunk underline = new Chunk(instituteName);
                underline.setUnderline(0.1f, -2f); //0.1 thick, -2 y-location
                paragraph.add(underline);
                document.add(paragraph);
                document.add(paragraph1);
                //heading end.....
                PdfPTable tab = new PdfPTable(10);
                float ff = tab.getWidthPercentage();

                tab.setWidthPercentage(100);

                float[] sglTblHdWidths = new float[10];
                sglTblHdWidths[0] = ((ff * 8) / 80);//rank
                sglTblHdWidths[1] = ((ff * 8) / 80);//roll
                sglTblHdWidths[2] = ((ff * 12) / 80);//Name
                sglTblHdWidths[3] = ((ff * 8) / 80);//total q
                sglTblHdWidths[4] = ((ff * 10) / 80);//att q
                sglTblHdWidths[5] = ((ff * 11) / 80);//corr
                sglTblHdWidths[6] = ((ff * 11) / 80);//incorr
                sglTblHdWidths[7] = ((ff * 13) / 80);//tot m
                sglTblHdWidths[8] = ((ff * 13) / 80);//obt mar
                sglTblHdWidths[9] = ((ff * 13) / 80);//obt mar
                tab.setWidths(sglTblHdWidths);
                float yp = tab.getWidthPercentage();
//            PdfPCell cell = new PdfPCell();
//        cell.setMinimumHeight(50);
//        cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//        cell.addElement(para);
//        tab1.addCell(cell);
//            tab1.setHorizontalAlignment(Element.ALIGN_CENTER);

//"Login ID", "Full Name", "Standard", "Division", "Academic Year", "Password"        
                tab.addCell(new Phrase("Rank",font3));
                tab.addCell(new Phrase("User ID.",font3));               
                tab.addCell(new Phrase("Student Name",font3));
                tab.addCell(new Phrase("Total Que.",font3));
                tab.addCell(new Phrase("Attempted Que.",font3));
                tab.addCell(new Phrase("Correct",font3));
                tab.addCell(new Phrase("Incorrect",font3));
                tab.addCell(new Phrase("Total Marks",font3));
                tab.addCell(new Phrase("Obtained Marks",font3));
                tab.addCell(new Phrase("Test Date",font3));

                for (int i = 0; i < rankBeanList.size(); i++) {
                     
                    int currentroll = rankBeanList.get(i).getRollNo();
                    
                        System.out.println("Pdf currentroll"+currentroll);
                   String StudentName=new StudentRegistrationOperation().getNameOfStudent1(currentroll);
                        System.out.println("Pdf StudentName*************"+StudentName);
                    
                    int testBeanId = rankBeanList.get(i).getTestbeanId();
                    ArrayList<QuestionBean> qb = new ArrayList<QuestionBean>();
                    qb =new SaveTestBlobOperation().getTestDetails(currentroll, testBeanId);
                    int c = 0, w = 0, una = 0;
                    for (int j = 0; j < qb.size(); j++) {
                        if (qb.get(j).getUserAnswer().equals(qb.get(j).getAnswer())) {
                            c++;
                        } else if (qb.get(j).getUserAnswer().equals("UnAttempted")) {
                            una++;
                        }
                    }
                    una = c + una;
                    w = qb.size() - una;
                    double total = (c * singlepmark);
                    double total1 = (w * singlenmark);
                    double Ftotal = total - total1;
                    Ftotal = new RestrctDoubleUpto2().round(Ftotal, 2);
                    int cnt = tblGenerateRanking.getRowCount(), roll = 0, rankofstud = 0;
                    ArrayList<Integer> donerow = new ArrayList<Integer>();
                    String getDate = "";
                    for (int j = 0; j < cnt; j++) {
                        roll = Integer.parseInt(tblGenerateRanking.getModel().getValueAt(j, 1).toString());
                        if (currentroll == roll) {
                            if (donerow.contains(j)) {
                            } else {
                                rankofstud = Integer.parseInt(tblGenerateRanking.getModel().getValueAt(j, 0).toString());
                                getDate = tblGenerateRanking.getModel().getValueAt(j, 4).toString();
                                donerow.add(j);
                            }
                        }
                    }
                    int att =rankBeanList.get(i).getCorrect_Questions()+rankBeanList.get(i).getIncorrect_Questions(); //c + w;
                    double to = rankBeanList.get(i).getTotalMark();//qb.size() * singlepmark;
                    double ObtainMark=rankBeanList.get(i).getObtainMark();
                    to = new RestrctDoubleUpto2().round(to, 2);
                    String value1 = rankofstud + "";//rank
                    String value2 = currentroll + "";//roll no
                    String value3 = StudentName + "";//StudentName
                    String value4 = qb.size() + "";//total que
                    String value5 = att + "";//att. que
                    String value6 = rankBeanList.get(i).getCorrect_Questions() + "";//corr
                    String value7 = rankBeanList.get(i).getIncorrect_Questions() + "";//incorr
                    String value8 = to + "";//tot marks
                    String value9 = ObtainMark + "";//obtained marks
                    String value10=rankBeanList.get(i).getTimeinstring();
                    tab.addCell(new Phrase(value1,font4));
                    tab.addCell(new Phrase(value2,font4));
                    tab.addCell(new Phrase(value3,font4));
                    tab.addCell(new Phrase(value4,font4));
                    tab.addCell(new Phrase(value5,font4));
                    tab.addCell(new Phrase(value6,font4));
                    tab.addCell(new Phrase(value7,font4));
                    tab.addCell(new Phrase(value8,font4));
                    tab.addCell(new Phrase(value9,font4));
                    tab.addCell(new Phrase(value10,font4));
                }
                document.add(tab);
                document.close();
//            createPdf(fna);
                File f = new File(fna);
                JLROpener.open(f);
            } else {
                JOptionPane.showMessageDialog(null, "Please enter name for PDF.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSummActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        new HomePage(1,"a").setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void BtnSendSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSendSMSActionPerformed
        // TODO add your handling code here:
                if (rankBeanList != null) {
                        String instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
                         new SendTestResult(rankBeanList,instituteName,this).setVisible(true);
                        this.setVisible(false);
                    }

    }//GEN-LAST:event_BtnSendSMSActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(RankGeneration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(RankGeneration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(RankGeneration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(RankGeneration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RankGeneration(null, 1).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnSendSMS;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSumm;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblSub;
    private javax.swing.JScrollPane panePhysics;
    private javax.swing.JTable tblGenerateRanking;
    // End of variables declaration//GEN-END:variables

    public Object GetData(JTable table, int row_index, int col_index) {
        return table.getModel().getValueAt(row_index, col_index);
    }

    public Object GetObj(JTable table, int row_index, int col_index) {
        return table.getModel().getValueAt(row_index, col_index);
    }

    public void createPdf(String filename) throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        ORANGEBorder event = new ORANGEBorder();
        writer.setPageEvent(event);
        // step 3
//        document.open();
//        // step 4
//        List<Integer> factors;
//        for (int i = 2; i < 301; i++) {
//            factors = getFactors(i);
//            if (factors.size() == 1) {
//                document.add(new Paragraph("This is a prime number!"));
//            }
//            for (int factor : factors) {
//                document.add(new Paragraph("Factor: " + factor));
//            }
//            document.newPage();
//        }
//        // step 5
//        document.close();

    }

    public class ORANGEBorder extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte canvas = writer.getDirectContent();
            Rectangle rect = document.getPageSize();
            rect.setBorder(Rectangle.BOX); // left, right, top, bottom border
            rect.setBorderWidth(5); // a width of 5 user units
            rect.setBorderColor(Color.ORANGE); // a red border
            rect.setUseVariableBorders(true); // the full width will be visible
            canvas.rectangle(rect);
        }
    }
}
