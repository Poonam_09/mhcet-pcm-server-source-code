/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.AcademicYearBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aniket
 */
public class AcademicYearOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public int getNewId(){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM ACADEMIC_YEAR_INFO ";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    
    public ArrayList<AcademicYearBean> getAcademicYearList() {
        ArrayList<AcademicYearBean> returnList = null;
        AcademicYearBean academicYearBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM ACADEMIC_YEAR_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next()) {
                academicYearBean = new AcademicYearBean();
                academicYearBean.setAcademicYearId(rs.getInt(1));
                academicYearBean.setName(rs.getString(2));
                
                if(returnList == null)
                    returnList = new ArrayList<AcademicYearBean>();

                returnList.add(academicYearBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public boolean insertAcademicYearBean(AcademicYearBean academicYearBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO ACADEMIC_YEAR_INFO VALUES(?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, academicYearBean.getAcademicYearId());
            ps.setString(2, academicYearBean.getName());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean updateAcademicYearBean(AcademicYearBean academicYearBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE ACADEMIC_YEAR_INFO SET ACADEMIC_YEAR = ? WHERE AY_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, academicYearBean.getName());
            ps.setInt(2, academicYearBean.getAcademicYearId());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean insertAcademicYearList(List<AcademicYearBean> academicYearList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO ACADEMIC_YEAR_INFO VALUES(?,?)";
            for(AcademicYearBean academicYearBean : academicYearList) { 
                ps = conn.prepareStatement(query);
                ps.setInt(1, academicYearBean.getAcademicYearId());
                ps.setString(2, academicYearBean.getName());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean updateAcademicYearList(List<AcademicYearBean> academicYearList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE ACADEMIC_YEAR_INFO SET ACADEMIC_YEAR = ? WHERE AY_ID = ?";
            for(AcademicYearBean academicYearBean : academicYearList) { 
                ps = conn.prepareStatement(query);
                ps.setString(1, academicYearBean.getName());
                ps.setInt(2, academicYearBean.getAcademicYearId());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
