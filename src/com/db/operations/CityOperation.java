/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.CityBean;
import com.bean.StateBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class CityOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<CityBean> getCityList() {
        ArrayList<CityBean> returnList = null;
        ArrayList<StateBean> stateList = new StateOperation().getStateList();
        
        if(stateList != null) {
            try {
                conn = new DbConnection().getConnection();
                String query = "SELECT * FROM CITY_INFO";
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                CityBean cityBean = null;
                while(rs.next()) {
                    cityBean = new CityBean();
                    cityBean.setCityId(rs.getInt(1));
                    cityBean.setCityName(rs.getString(2));
                    for(StateBean stateBean : stateList) {
                        if(rs.getInt(3) == stateBean.getStateId()) {
                            cityBean.setStateBean(stateBean);
                            break;
                        }
                    }
                    if(returnList == null)
                        returnList = new ArrayList<CityBean>();

                    returnList.add(cityBean);
                }
            } catch(Exception ex) {
                returnList = null;
                ex.printStackTrace();
            } finally {
                sqlClose();
            }
        }
        
        return returnList;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
