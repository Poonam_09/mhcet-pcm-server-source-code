/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.Model.SwapData;
import com.bean.CityBean;
import com.bean.ProductBean;
import com.bean.RegistrationBean;
import com.db.DbConnection;
import com.id.operations.NewIdOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class RegistrationOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<RegistrationBean> getRegistrationInfoList() {
        ArrayList<RegistrationBean> returnList = null;
        RegistrationBean registrationBean = null;
        SwapData swapData = null;
        ArrayList<CityBean> cityList = null;
        ArrayList<ProductBean> productList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM REGISTRATION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            String exDate;
            while(rs.next()) {
                if(swapData == null)
                    swapData = new SwapData();
                if(cityList == null)
                    cityList = new CityOperation().getCityList();
                if(productList == null)
                    productList = new ProductOperation().getProductList();
                registrationBean = new RegistrationBean();
                registrationBean.setRegId(rs.getInt(1));
                registrationBean.setCustName(rs.getString(2));
                registrationBean.setCustMobile(rs.getString(3));
                registrationBean.setMailId(rs.getString(4));
                registrationBean.setAddress(rs.getString(5));
                for(CityBean cityBean : cityList) {
                    if(cityBean.getCityId() == rs.getInt(6)) {
                        registrationBean.setCityBean(cityBean);
                        break;
                    }
                }
                registrationBean.setPinCode(rs.getString(7));
                registrationBean.setSellerName(rs.getString(8));
                registrationBean.setSellerMobile(rs.getString(9));
                String instName = rs.getString(10);
                instName = instName.replace("â€™", "'");       
                registrationBean.setInstituteName(instName);
                for(ProductBean productBean : productList) {
                    if(productBean.getProductId() == rs.getInt(11)) {
                        registrationBean.setProductBean(productBean);
                        break;
                    }
                }
                registrationBean.setCpuId(rs.getString(12));
                registrationBean.setCdKey(rs.getString(13));
                registrationBean.setPinKey(rs.getString(14));
                registrationBean.setActivationDate(swapData.getUnSwappedData(rs.getString(15)));
                exDate = rs.getString(16);
                if(exDate != "" && exDate != null)
                    registrationBean.setExpiryDate(swapData.getUnSwappedData(exDate));
                else
                    registrationBean.setExpiryDate(exDate);
                registrationBean.setActiveStatus(rs.getBoolean(17));
                
                if(returnList == null)
                    returnList = new ArrayList<RegistrationBean>();
                returnList.add(registrationBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public RegistrationBean getLastRegistrationInfoBean() {
        RegistrationBean returnBean = null;
        SwapData swapData = null;
        ArrayList<CityBean> cityList = null;
        ArrayList<ProductBean> productList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM REGISTRATION_INFO WHERE REG_ID = (SELECT MAX(REG_ID) FROM REGISTRATION_INFO)";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            String exDate;
            while(rs.next()) {
                if(swapData == null)
                    swapData = new SwapData();
                if(cityList == null)
                    cityList = new CityOperation().getCityList();
                if(productList == null)
                    productList = new ProductOperation().getProductList();
                returnBean = new RegistrationBean();
                returnBean.setRegId(rs.getInt(1));
                returnBean.setCustName(rs.getString(2));
                returnBean.setCustMobile(rs.getString(3));
                returnBean.setMailId(rs.getString(4));
                returnBean.setAddress(rs.getString(5));
                for(CityBean cityBean : cityList) {
                    if(cityBean.getCityId() == rs.getInt(6)) {
                        returnBean.setCityBean(cityBean);
                        break;
                    }
                }
                returnBean.setPinCode(rs.getString(7));
                returnBean.setSellerName(rs.getString(8));
                returnBean.setSellerMobile(rs.getString(9));
                String instName = rs.getString(10);
                instName = instName.replace("â€™", "'");       
                returnBean.setInstituteName(instName);
                for(ProductBean productBean : productList) {
                    if(productBean.getProductId() == rs.getInt(11)) {
                        returnBean.setProductBean(productBean);
                        break;
                    }
                }
                returnBean.setCpuId(rs.getString(12));
                returnBean.setCdKey(rs.getString(13));
                returnBean.setPinKey(rs.getString(14));
                returnBean.setActivationDate(swapData.getUnSwappedData(rs.getString(15)));
                exDate = rs.getString(16);
                if(exDate != "" && exDate != null)
                    returnBean.setExpiryDate(swapData.getUnSwappedData(exDate));
                else
                    returnBean.setExpiryDate(exDate);
                returnBean.setActiveStatus(rs.getBoolean(17));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnBean = null;
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    public boolean insertRegistrationFirstPart(RegistrationBean infoBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            int maxId = new NewIdOperation().getNewId("REGISTRATION_INFO");
            infoBean.setRegId(maxId);
            String query = "INSERT INTO REGISTRATION_INFO "
                    + "(REG_ID,CUST_NAME,MBL_NUM,MAIL_ID,ADDRESS,CITY_ID,PIN_CODE,SELLER_NAME,SELLER_MBL,INST_NAME,PRD_ID,CPU_ID,ACT_DATE,ACT_STATUS)"
                    + "Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, infoBean.getRegId());
            ps.setString(2, infoBean.getCustName().trim());
            ps.setString(3, infoBean.getCustMobile().trim());
            ps.setString(4, infoBean.getMailId().trim());
            ps.setString(5, infoBean.getAddress().trim());
            ps.setInt(6, infoBean.getCityBean().getCityId());
            ps.setString(7,infoBean.getPinCode().trim());
            ps.setString(8, infoBean.getSellerName().trim());
            ps.setString(9, infoBean.getSellerMobile().trim());
            ps.setString(10,infoBean.getInstituteName().trim());
            ps.setInt(11, infoBean.getProductBean().getProductId());
            ps.setString(12, infoBean.getCpuId().trim());
            ps.setString(13, new SwapData().getSwappedData(infoBean.getActivationDate().trim()));
            ps.setBoolean(14, false);
            
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean insertRegistrationSecondPart(RegistrationBean infoBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE REGISTRATION_INFO SET EXP_DATE = ? , CD_KEY = ? , PIN_KEY = ? , ACT_STATUS = ? WHERE REG_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, new SwapData().getSwappedData(infoBean.getExpiryDate().trim()));
            ps.setString(2, infoBean.getCdKey().trim());
            ps.setString(3, infoBean.getPinKey().trim());
            ps.setBoolean(4, true);
            ps.setInt(5,infoBean.getRegId());
            
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public void deleteAllRow() {
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM REGISTRATION_INFO";
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public void deleteLastRow() {
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM REGISTRATION_INFO WHERE REG_ID = (SELECT MAX(REG_ID) FROM REGISTRATION_INFO)";
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public void deleteUnWantedRow() {
        try {
            ArrayList<RegistrationBean> registrationInfoList = getRegistrationInfoList();
            int listSize = registrationInfoList.size();
            boolean deleteStatus = false;
            if(listSize > 1) {
                if(registrationInfoList.get(listSize-2).getExpiryDate() == null)
                    deleteStatus = true;
            }
            if(deleteStatus) {
                conn = new DbConnection().getConnection();
                String query = "DELETE FROM REGISTRATION_INFO WHERE REG_ID = ?";
                ps = conn.prepareStatement(query);
                ps.setInt(1, listSize-1);
                ps.executeUpdate();
                query = "UPDATE REGISTRATION_INFO SET REG_ID = ? WHERE REG_ID = ?";
                ps = conn.prepareStatement(query);
                ps.setInt(1, listSize-1);
                ps.setInt(2, listSize);
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public boolean updateExpiryStatus(RegistrationBean infoBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE REGISTRATION_INFO SET ACT_STATUS = ? WHERE REG_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setBoolean(1, false);
            ps.setInt(2, infoBean.getRegId());
            ps.executeUpdate(); 
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean updateExpiry(RegistrationBean infoBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE REGISTRATION_INFO SET EXP_DATE = ? WHERE REG_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, null);
            ps.setInt(2, infoBean.getRegId());
            ps.executeUpdate(); 
            
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
