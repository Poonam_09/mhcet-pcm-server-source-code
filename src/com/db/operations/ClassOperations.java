/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ClassBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aniket
 */
public class ClassOperations {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public int getNewId(){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM CLASS_INFO ";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    public ArrayList<ClassBean> getClassList() 
    {
        ArrayList<ClassBean> returnList = null;
        ClassBean classBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM CLASS_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next()) {
                classBean = new ClassBean();
                classBean.setClassId(rs.getInt(1));
                classBean.setName(rs.getString(2));
                
                if(returnList == null)
                    returnList = new ArrayList<ClassBean>();

                returnList.add(classBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public boolean insertClassBean(ClassBean classBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO CLASS_INFO VALUES(?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, classBean.getClassId());
            ps.setString(2, classBean.getName());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean updateClassBean(ClassBean classBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE CLASS_INFO SET STANDARD = ? WHERE CLASS_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, classBean.getName().trim());
            ps.setInt(2, classBean.getClassId());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean insertClassList(List<ClassBean> classList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO CLASS_INFO VALUES(?,?)";
            for(ClassBean classBean : classList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, classBean.getClassId());
                ps.setString(2, classBean.getName().trim());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean updateClassList(List<ClassBean> classList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE CLASS_INFO SET STANDARD = ? WHERE CLASS_ID = ?";
            for(ClassBean classBean : classList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, classBean.getName().trim());
                ps.setInt(2, classBean.getClassId());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
