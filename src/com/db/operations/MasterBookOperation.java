/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.MasterBookBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class MasterBookOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<MasterBookBean> getBookList() {
        ArrayList<MasterBookBean> returnList = null;
        conn = new DbConnection().getConnection();
        try {
            String query = "SELECT * FROM MASTER_BOOK_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            MasterBookBean bookBean = null;
            while(rs.next()) {
                bookBean = new MasterBookBean();
                bookBean.setBookId(rs.getInt(1));
                bookBean.setBookName(rs.getString(2));
                bookBean.setBookAuthorName(rs.getString(3));
                
                if(returnList == null)
                    returnList = new ArrayList<MasterBookBean>();
                
                returnList.add(bookBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }    
        return returnList;
    }
    
    public boolean insertBook(MasterBookBean bookBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO MASTER_BOOK_INFO VALUES(?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, bookBean.getBookId());
            ps.setString(2, bookBean.getBookName());
            ps.setString(3, bookBean.getBookAuthorName());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    public boolean updateBook(MasterBookBean bookBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "UPDATE MASTER_BOOK_INFO SET BOOK_NAME = ?,BOOK_AUTHOR = ? WHERE BOOK_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, bookBean.getBookName());
            ps.setString(2, bookBean.getBookAuthorName());
            ps.setInt(3, bookBean.getBookId());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
