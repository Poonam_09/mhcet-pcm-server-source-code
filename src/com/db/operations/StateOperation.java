/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.StateBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class StateOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<StateBean> getStateList() {
        ArrayList<StateBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM STATE_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            StateBean stateBean = null;    
            while(rs.next()) {
                stateBean = new StateBean();
                stateBean.setStateId(rs.getInt(1));
                stateBean.setStateName(rs.getString(2));

                if(returnList == null)
                    returnList = new ArrayList<StateBean>();

                returnList.add(stateBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
