/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class SendSMSBean {
    private  ArrayList<StudentBean> studentBeanList;
    private int Rank;
    private ArrayList<RankBean> rankBeanList ;

    public ArrayList<StudentBean> getStudentBeanList() {
        return studentBeanList;
    }

    public void setStudentBeanList(ArrayList<StudentBean> studentBeanList) {
        this.studentBeanList = studentBeanList;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    public ArrayList<RankBean> getRankBeanList() {
        return rankBeanList;
    }

    public void setRankBeanList(ArrayList<RankBean> rankBeanList) {
        this.rankBeanList = rankBeanList;
    }
    
    
}
