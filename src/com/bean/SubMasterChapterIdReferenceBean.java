/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class SubMasterChapterIdReferenceBean {
    private int subMasterChapterId;
    private String masterChapterIds;
    private int newChapterID;

    public int getSubMasterChapterId() {
        return subMasterChapterId;
    }

    public void setSubMasterChapterId(int subMasterChapterId) {
        this.subMasterChapterId = subMasterChapterId;
    }

    public String getMasterChapterIds() {
        return masterChapterIds;
    }

    public void setMasterChapterIds(String masterChapterIds) {
        this.masterChapterIds = masterChapterIds;
    }

    public int getNewChapterID() {
        return newChapterID;
    }

    public void setNewChapterID(int newChapterID) {
        this.newChapterID = newChapterID;
    }
}
