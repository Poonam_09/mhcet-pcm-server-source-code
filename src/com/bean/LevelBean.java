/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class LevelBean implements Comparable<LevelBean>{
    private int questionId;
    private int hintSize;
    private int questionIndex;
    
    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getHintSize() {
        return hintSize;
    }

    public void setHintSize(int hintSize) {
        this.hintSize = hintSize;
    }

    public int getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(int questionIndex) {
        this.questionIndex = questionIndex;
    }
    
    @Override
    public int compareTo(LevelBean levelBean) {
        if(hintSize == levelBean.hintSize) 
            return 0;
        else if(hintSize > levelBean.hintSize)
            return 1;
        else
            return -1;
    }
}
