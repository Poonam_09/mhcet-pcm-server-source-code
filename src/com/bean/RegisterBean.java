/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author admin
 */
public class RegisterBean {
    private String cdKey;
    private String pin;
    private String motherboardSerialNo;
    private String registerTime;
    private String collageName;

    public String getCdKey() {
        return cdKey;
    }

    public void setCdKey(String cdKey) {
        this.cdKey = cdKey;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getMotherboardSerialNo() {
        return motherboardSerialNo;
    }

    public void setMotherboardSerialNo(String motherboardSerialNo) {
        this.motherboardSerialNo = motherboardSerialNo;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getCollageName() {
        return collageName;
    }

    public void setCollageName(String collageName) {
        this.collageName = collageName;
    }
}
