/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class GroupBean {
    private int groupId;
    private String groupName;
    private ArrayList<MasterSubjectBean> subjectList;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<MasterSubjectBean> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<MasterSubjectBean> subjectList) {
        this.subjectList = subjectList;
    }
    
}
