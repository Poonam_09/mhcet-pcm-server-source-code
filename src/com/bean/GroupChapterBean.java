/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class GroupChapterBean {
    private int chapterId;
    private String chapterName;
    private MasterSubjectBean subjectBean;
    private int weightage;
    private MasterChapterBean masterChapterBean;
    private GroupBean groupBean;

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public MasterSubjectBean getSubjectBean() {
        return subjectBean;
    }

    public void setSubjectBean(MasterSubjectBean subjectBean) {
        this.subjectBean = subjectBean;
    }

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }

    public MasterChapterBean getMasterChapterBean() {
        return masterChapterBean;
    }

    public void setMasterChapterBean(MasterChapterBean masterChapterBean) {
        this.masterChapterBean = masterChapterBean;
    }

    public GroupBean getGroupBean() {
        return groupBean;
    }

    public void setGroupBean(GroupBean groupBean) {
        this.groupBean = groupBean;
    }
}
