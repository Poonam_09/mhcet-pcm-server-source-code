/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ui.support.pages;

/**
 *
 * @author Administrator
 */
public class NoQuestionPanel extends javax.swing.JPanel {

    public NoQuestionPanel() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lblQuestionNo = new javax.swing.JLabel();

        setBackground(new java.awt.Color(233, 238, 255));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setAutoscrolls(true);
        setPreferredSize(new java.awt.Dimension(1030, 440));

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQuestionNo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/NoQuestionFound.png"))); // NOI18N
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblQuestionNo)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblQuestionNo)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel lblQuestionNo;
    // End of variables declaration//GEN-END:variables
}
