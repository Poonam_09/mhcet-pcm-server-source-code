/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ui.support.pages;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 *
 * @author Aniket
 */
public class ChangeClipBoardData {
    public void Change() throws UnsupportedFlavorException, IOException {
        Transferable str = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
        String que = (String) str.getTransferData(DataFlavor.stringFlavor);
        System.out.println("old: " + que);
        que = que.replace("\\dfrac", "\\frac");
        que = que.replace("\\frac", "\\dfrac");
        char c[] = new char[que.length()];
        c = que.toCharArray();
        char temp[] = new char[que.length() + 7];
        for (int i = 0, j = 0; i < que.length(); i++, j++) {
            if (c[i] == '\\') {
                if (c[i + 1] == '[') {
                    temp[j] = '}';
                    j++;
                    temp[j] = ' ';
                    j++;
                    temp[j] = '$';
                    i++;
                } else {
                    temp[j] = c[i];
                }
            } else {
                temp[j] = c[i];
            }
        }
        c = temp;
        for (int i = 0, j = 0; i < que.length(); i++, j++) {
            if (c[i] == '\\') {
                if (c[i + 1] == ']') {
                    temp[j] = '$';
                    j++;
                    temp[j] = ' ';
                    j++;
                    temp[j] = '\\';
                    j++;
                    temp[j] = 'm';
                    j++;
                    temp[j] = 'b';
                    j++;
                    temp[j] = 'o';
                    j++;
                    temp[j] = 'x';
                    j++;
                    temp[j] = '{';
                    i++;
                } else {
                    temp[j] = c[i];
                }
            } else {
                temp[j] = c[i];
            }
        }
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == ' ') {
                temp[i] = ' ';
            }
        }
        String ret = new String(temp);
        ret = ret.replaceAll("\\n", " ");
        ret = ret.trim();
        System.out.println("new: " + ret);
        StringSelection sss = new StringSelection(ret);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sss, sss);
    }
}