/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.PrintPdfTeacherCopyBean;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Aniket
 */
public class PrintTeacherCopy {
    public void setPrintPdf(String teacherMainString,String instituteName,String timeDuration,int totalMarks,String headerString,String fileLocation) {
        PrintPdfTeacherCopyBean pdfTeacherCopyBean = new PrintPdfTeacherCopyBean();
        pdfTeacherCopyBean.setWaterMarkText(instituteName.trim());
        pdfTeacherCopyBean.setFooterLeft(getFooterRight());
        pdfTeacherCopyBean.setInstituteName(instituteName.trim());
        pdfTeacherCopyBean.setHeaderText(headerString);
        pdfTeacherCopyBean.setTimeDuration(timeDuration);
        pdfTeacherCopyBean.setTotalMarks(""+totalMarks);
        pdfTeacherCopyBean.setMainString(teacherMainString);
        new ExportProcess().createFormatTeacherCopyPdf(pdfTeacherCopyBean, fileLocation);
    }
    
    private String getFooterRight() {
        String returnValue = "";
        DateFormat dateFormat = new SimpleDateFormat("E dd-MMM-yyyy");
        returnValue = dateFormat.format(System.currentTimeMillis());
        return returnValue;
    }
}
