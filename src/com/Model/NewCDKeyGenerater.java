/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class NewCDKeyGenerater {
    public String getCDKey(String productName,String cpuId,String activationDate) {
        String returnValue = productName + activationDate + cpuId;
        HashCodeGenerator hc = new HashCodeGenerator();
        returnValue = returnValue.toLowerCase();
        returnValue = hc.pinHash(returnValue, "MD5") + hc.pinHash(returnValue, "MD2") + hc.pinHash(returnValue,"SHA1");
        
        returnValue = ""
                    + returnValue.charAt(13)
                    + returnValue.charAt(28)
                    + returnValue.charAt(88)
                    + returnValue.charAt(94)
                    + returnValue.charAt(47)
                    + returnValue.charAt(62)
                    + returnValue.charAt(4)
                    + returnValue.charAt(55)
                    + returnValue.charAt(22)
                    + returnValue.charAt(1);
        returnValue = changeChars(returnValue);
        returnValue = returnValue.toUpperCase();
        return returnValue;
    }
    
    private String changeChars(String str) {
        ArrayList<Character> chIndexList = new ArrayList<Character>();
        for(int i=0;i<10;i++)
            chIndexList.add(Character.forDigit(i, 10));
        chIndexList.add('a');chIndexList.add('b');chIndexList.add('c');
        chIndexList.add('d');chIndexList.add('e');chIndexList.add('f');
        
        ArrayList<Character> chCharList = new ArrayList<Character>();
        chCharList.add('s');chCharList.add('m'); chCharList.add('h');chCharList.add('w');
        chCharList.add('t');chCharList.add('l');chCharList.add('u');chCharList.add('4');
        chCharList.add('p'); chCharList.add('g');chCharList.add('1');chCharList.add('a');
        chCharList.add('x');chCharList.add('q');chCharList.add('d'); chCharList.add('7');
    
        String returnVal = "";
        for(int i=0;i<str.length();i++) {
            char ch = str.charAt(i);
            if(ch != '-') {
                int index = chIndexList.indexOf(ch);
                returnVal += ""+chCharList.get(index);
            } else {
                returnVal += "-";
            }
        }
        return returnVal;
    }
}
