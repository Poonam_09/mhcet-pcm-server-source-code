/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Aniket
 */
public class WordImageProcessing {
    
    public boolean writeWordImage(BufferedImage imageFile,String fileName) {
        boolean returnValue = false;
        try {
            int destWidth = (int) (imageFile.getWidth() * 0.7);
            int destHeight = (int) (imageFile.getHeight() * 0.7);
            BufferedImage dest = new BufferedImage(destWidth, destHeight,
                    BufferedImage.TYPE_INT_ARGB);
            
            // Paint source image into the destination, scaling as needed
            Graphics2D graphics2D = dest.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(imageFile, 0, 0, destWidth, destHeight, null);
            graphics2D.dispose();
            
            // Save destination image
            ImageIO.write(dest, "PNG", new File("temp/" + fileName +".png"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }
}
