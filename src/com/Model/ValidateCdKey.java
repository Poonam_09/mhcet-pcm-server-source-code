/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.RegistrationBean;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Aniket
 */
public class ValidateCdKey {
    private SwappingKeyRetrive swp;
    
    public ValidateCdKey() {
        swp = new SwappingKeyRetrive();
    }
    
    public boolean validateKey(RegistrationBean infoBean) {
        boolean returnValue = false;
        String productName = infoBean.getProductBean().getProductName().trim() + " " + infoBean.getProductBean().getProductTypeBean().getProductType().trim();
        String mobileNumber = infoBean.getCustMobile().trim();
        String machineCdKey1 = new NewCDKeyGenerater().getCDKey(productName, infoBean.getCpuId().trim(), getSystemDate(0));
        String machineCdKey2 = new NewCDKeyGenerater().getCDKey(productName, infoBean.getCpuId().trim(), getSystemDate(1));
        String userCdKey = infoBean.getCdKey().trim();
        userCdKey = swp.swapCdKey(userCdKey, mobileNumber);
        ArrayList<Integer> primeIndexList = getAlgoBasedList(mobileNumber);
        String tempPrime = "";
        String tempCdKey = "";
        for(int i=0;i<userCdKey.length();i++) {
            if(i!=4 && i!=9 && i!=14) {
                if(primeIndexList.contains(i)) {
                    tempPrime += userCdKey.charAt(i);
                } else {
                    tempCdKey += userCdKey.charAt(i);
                }
            }
        }
        
//        for(int index : primeIndexList) {
//           tempPrime += userCdKey.charAt(index);
//        } 
        int primeNumber = 0;
        try {
            primeNumber = Integer.parseInt(tempPrime);
        } catch(Exception ex) {
            primeNumber = 0;
        }
        if(primeNumber == 0) {
            returnValue = false;
        } else {
            if(checkPrime(primeNumber)) {
                if(tempCdKey.equalsIgnoreCase(machineCdKey1) || tempCdKey.equalsIgnoreCase(machineCdKey2))
                    returnValue = true;
                else
                    returnValue = false;
            } else {
                returnValue = false;
            }
        }
        
        return returnValue;
    }
    
    
    private ArrayList<Integer> getAlgoBasedList(String mobileNumber) {
        ArrayList<Integer> returnList = null;
        if(getOddEven(""+mobileNumber.charAt(2)+mobileNumber.charAt(1)+mobileNumber.charAt(8)+mobileNumber.charAt(3)).equalsIgnoreCase("Even"))
            returnList = getAlgoValueList(getDigitTotal(""+mobileNumber.charAt(8)+mobileNumber.charAt(1)+mobileNumber.charAt(0)+mobileNumber.charAt(9)+mobileNumber.charAt(5)));
        else
            returnList = getAlgoValueList(getDigitTotal(""+mobileNumber.charAt(7)+mobileNumber.charAt(1)+mobileNumber.charAt(2)+mobileNumber.charAt(8)+mobileNumber.charAt(6))); 
        return returnList;
    }
    
    private ArrayList<Integer> getAlgoValueList(int algoValue) {
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        if(algoValue == 0) {
            returnList.add(4);returnList.add(11);returnList.add(17);
            returnList.add(2);returnList.add(6);returnList.add(10);
        } else if(algoValue == 1) {
            returnList.add(10);returnList.add(2);returnList.add(8);
            returnList.add(18);returnList.add(13);returnList.add(15);
        } else if(algoValue == 2) {
            returnList.add(1);returnList.add(3);returnList.add(11);
            returnList.add(17);returnList.add(7);returnList.add(0);
        } else if(algoValue == 3) {
            returnList.add(6);returnList.add(16);returnList.add(18);
            returnList.add(10);returnList.add(2);returnList.add(15);
        } else if(algoValue == 4) {
            returnList.add(0);returnList.add(11);returnList.add(2);
            returnList.add(15);returnList.add(8);returnList.add(5);
        } else if(algoValue == 5) {
            returnList.add(13);returnList.add(10);returnList.add(18);
            returnList.add(16);returnList.add(2);returnList.add(1);
        } else if(algoValue == 6) {
            returnList.add(2);returnList.add(6);returnList.add(10);
            returnList.add(3);returnList.add(8);returnList.add(12);
        } else if(algoValue == 7) {
            returnList.add(12);returnList.add(18);returnList.add(16);
            returnList.add(6);returnList.add(8);returnList.add(2);
        } else if(algoValue == 8) {
            returnList.add(1);returnList.add(12);returnList.add(15);
            returnList.add(5);returnList.add(17);returnList.add(3);
        } else if(algoValue == 9) {
            returnList.add(8);returnList.add(0);returnList.add(16);
            returnList.add(7);returnList.add(12);returnList.add(2);
        } 
        return returnList;
    }
    
    private int getDigitTotal(String str) {
        char[] chArray = str.toCharArray();
        int sum = 0;
        for (char ch : chArray) {
            sum += Integer.parseInt("" + ch);
        }
        while (true) {
            if (sum < 10) {
                break;
            } else {
                sum = getDigitCal(sum);
            }
        }
        return sum;
    }
   
    private int getDigitCal(int no) {
        int sum = 0;
        while (no != 0) {
            sum += no % 10;
            no /= 10;
        }
        return sum;
    }
    
    private String getOddEven(String str) {
        char[] chArray = str.toCharArray();
        int sum = 0;
        for (char ch : chArray) {
            sum += Integer.parseInt("" + ch);
        }
        if (sum % 2 == 0) {
            return "Even";
        } else {
            return "Odd";
        }
    }
    
    private boolean checkPrime(int n){  
        boolean returnValue = false;
        int i,m = 0,flag = 0;      
        m = n/2;      
        if(n == 0|| n == 1) {  
            returnValue = false;
        } else {  
            for(i=2;i<=m;i++) {       
                if(n%i == 0) {      
                    System.out.println(n+" is not prime number");      
                    flag=1;      
                    break;      
                }
            }
            
            if(flag == 0)
                returnValue = true;
            else
                returnValue = false;
        }
        return returnValue;
    }
    
    private String getSystemDate(int count) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, count);
        String returnDate = dateFormat.format(cal.getTimeInMillis());
        return returnDate;
    }
}
