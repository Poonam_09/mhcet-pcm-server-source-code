/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author hari om
 */
public class Utilities {
    public String dateToString(Date date,String format){
        DateFormat df = new SimpleDateFormat(format); //"dd-MM-yyyy"
        String strDate = df.format(date);
        return strDate;
    }
    
    public Date stringToDate(String strDate,String format){//"dd-MMM-yyyy"
        DateFormat formatter ;        
        formatter = new SimpleDateFormat(format);
        try {
            Date date = formatter.parse(strDate);
            return date;
        } 
        catch (ParseException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void clearTable(JTable table){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int rowCount = model.getRowCount();
        for(int i=rowCount-1;i>=0;i--){
            model.removeRow(i);
        }
    }
    
//    void loadTaxes(){
//        taxNames = new ArrayList<String>();
//        taxIds = new ArrayList<Integer>();
//        cmbTaxNames.removeAllItems();
//        ArrayList<TaxBean> allTaxes = new Service().getAllTaxes();
//        for(int i=0;i<allTaxes.size();i++){
//            TaxBean taxBean = allTaxes.get(i);
//            taxNames.add(taxBean.getTaxName());
//            cmbTaxNames.addItem(taxBean.getTaxName());
//            taxIds.add(taxBean.getTaxId());
//        }
//    }
}
