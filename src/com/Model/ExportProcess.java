/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.LatexProcessing.GenrateOMRLatexCode;
import com.Pdf.Generation.PdfFinalPanel;
import com.Word.Generation.WordFinalPanel;
import com.bean.PrintPdfFormatFirstBean;
import com.bean.PrintPdfFormatSecondBean;
import com.bean.PrintPdfTeacherCopyBean;
import com.bean.PrintWordBean;
import com.pages.HomePage;
import de.nixosoft.jlr.JLRConverter;
import de.nixosoft.jlr.JLRGenerator;
import de.nixosoft.jlr.JLROpener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class ExportProcess {
    
    public void createFormatFirstPdf(PrintPdfFormatFirstBean printPdfFormatFirstBean,String fileLocation,PdfFinalPanel pdfFinalPanel) {
        String Directory=fileLocation.split(",")[0];
        String fileName=fileLocation.split(",")[1];
        
        //System.out.println("Directory"+Directory);
        //System.out.println("fileName"+fileName);
        
        File workingDirectory = new File(new ProcessManager().getProcessPath());
        //System.out.println("workingDirectory ="+workingDirectory);
        
        File template = new File(workingDirectory.getAbsolutePath() + File.separator + "PdfFrmtFirst.tex");
       // System.out.println("template" +template);

        File tempDir = new File(workingDirectory.getAbsolutePath() + File.separator + "temp");
       // System.out.println("tempDir" +tempDir);
        
        if (!tempDir.isDirectory()) {
            tempDir.mkdir();
        }

        File invoice1 = new File(tempDir.getAbsolutePath() + File.separator + "PdfFrmtFirst.tex");

         try {
            JLRConverter converter = new JLRConverter(workingDirectory);
            converter.replace("pageType", printPdfFormatFirstBean.getPageType());
            converter.replace("headL", printPdfFormatFirstBean.getHeaderLeft());
            converter.replace("headC", printPdfFormatFirstBean.getHeaderCenter());
            converter.replace("headR", printPdfFormatFirstBean.getHeaderRight());
            converter.replace("footL", printPdfFormatFirstBean.getFooterLeft());
            converter.replace("footC", printPdfFormatFirstBean.getFooterCenter());
            converter.replace("footR", printPdfFormatFirstBean.getFooterRight());
            converter.replace("pageBorderCode", printPdfFormatFirstBean.getPageBorder());
            converter.replace("waterMarkTextImage", printPdfFormatFirstBean.getWaterMark());
            converter.replace("headerFooterOnFirstPage", printPdfFormatFirstBean.getHeaderFooterOnFirstPage()); 
            converter.replace("headerText", printPdfFormatFirstBean.getClgDepartment());
            converter.replace("subjectMarksTotal", printPdfFormatFirstBean.getFourFields());
            converter.replace("rollDivisionTime", printPdfFormatFirstBean.getRollDivisionTime());
            converter.replace("paperName", printPdfFormatFirstBean.getPaperName());
            converter.replace("mainString", printPdfFormatFirstBean.getMainString());
              
            ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();
		
            ArrayList<String> subservice1 = new ArrayList<String>();
            ArrayList<String> subservice2 = new ArrayList<String>();
            ArrayList<String> subservice3 = new ArrayList<String>();
		
            services.add(subservice1);
            services.add(subservice2);
            services.add(subservice3);		
		
            converter.replace("services", services);
            
            if (!converter.parse(template, invoice1)) {
                System.out.println(converter.getErrorMessage());
            }

            services = new ArrayList<ArrayList<String>>();
		
            subservice1 = new ArrayList<String>();
            subservice2 = new ArrayList<String>();
            subservice3 = new ArrayList<String>();
            
            converter.replace("services", services);

            File desktop = new File(Directory);
            JLRGenerator pdfGen = new JLRGenerator();   
            System.out.println("Start");
            if (!pdfGen.generate(invoice1, desktop, workingDirectory)) { 
                System.out.println(pdfGen.getErrorMessage());
            }
            System.out.println("End");
            
            File oldFileName = new File(Directory + "/PdfFrmtFirst.pdf");
            File newFileName = new File(Directory + "/" + fileName+".pdf");
            
            System.out.println("oldFileName********="+Directory + "/PdfFrmtSecond.pdf");
            
            System.out.println("newFileName*******="+Directory + "/" + fileName+".pdf");
            
             try {
                 if (oldFileName.renameTo(newFileName)) {
                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf created successfully");
                     System.out.println("File renamed successfull !");
                     JLROpener.open(newFileName);
                 } else {
                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf failed to create");
                     System.out.println("File rename operation failed !");
                 }

             } catch (Exception e) {
                 e.printStackTrace();
             }
            
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public void createFormatSecondPdf(PrintPdfFormatSecondBean printPdfFormatSecondBean,String fileLocation,PdfFinalPanel pdfFinalPanel) {
        String Directory=fileLocation.split(",")[0];
        String fileName=fileLocation.split(",")[1];
        
        File workingDirectory = new File(new ProcessManager().getProcessPath());
        
        File template = new File(workingDirectory.getAbsolutePath() + File.separator + "PdfFrmtSecond.tex");
        File tempDir = new File(workingDirectory.getAbsolutePath() + File.separator + "temp");
        if (!tempDir.isDirectory()) {
            tempDir.mkdir();
        }

        File invoice1 = new File(tempDir.getAbsolutePath() + File.separator + "PdfFrmtSecond.tex");

        try {
            JLRConverter converter = new JLRConverter(workingDirectory);
            converter.replace("pageType", printPdfFormatSecondBean.getPageType());
            converter.replace("waterMarkTextImage", printPdfFormatSecondBean.getWaterMark());
            converter.replace("pageBorderCode", printPdfFormatSecondBean.getPageBorder());
            converter.replace("headL", printPdfFormatSecondBean.getHeaderLeft());
            converter.replace("headC", printPdfFormatSecondBean.getHeaderCenter());
            converter.replace("headR", printPdfFormatSecondBean.getHeaderRight());
            converter.replace("footL", printPdfFormatSecondBean.getFooterLeft());
            converter.replace("footC", printPdfFormatSecondBean.getFooterCenter());
            converter.replace("footR", printPdfFormatSecondBean.getFooterRight());
            converter.replace("headerFooterOnFirstPage", printPdfFormatSecondBean.getHeaderFooterOnFirstPage()); 
            converter.replace("instituteName", printPdfFormatSecondBean.getInstituteName());
            converter.replace("headerText", printPdfFormatSecondBean.getHeaderText());
            converter.replace("timeDuration", printPdfFormatSecondBean.getTimeDuration());
            converter.replace("paperType", printPdfFormatSecondBean.getPaperType());
            converter.replace("totalMarks", printPdfFormatSecondBean.getTotalMarks());
            converter.replace("mainString", printPdfFormatSecondBean.getMainString());
              
            ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();
		
            ArrayList<String> subservice1 = new ArrayList<String>();
            ArrayList<String> subservice2 = new ArrayList<String>();
            ArrayList<String> subservice3 = new ArrayList<String>();
		
            services.add(subservice1);
            services.add(subservice2);
            services.add(subservice3);		
		
            converter.replace("services", services);
            
            if (!converter.parse(template, invoice1)) {
                System.out.println(converter.getErrorMessage());
            }

            services = new ArrayList<ArrayList<String>>();
		
            subservice1 = new ArrayList<String>();
            subservice2 = new ArrayList<String>();
            subservice3 = new ArrayList<String>();
            
            converter.replace("services", services);

            File desktop = new File(Directory);
            JLRGenerator pdfGen = new JLRGenerator();   
            System.out.println("Start");
            if (!pdfGen.generate(invoice1, desktop, workingDirectory)) { 
                System.out.println(pdfGen.getErrorMessage());
            }
            System.out.println("End");
            
            File oldFileName = new File(Directory + "/PdfFrmtSecond.pdf");
            File newFileName = new File(Directory + "/" + fileName+".pdf");
            
            
            
             try {
                 if (oldFileName.renameTo(newFileName)) {
                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf created successfully");
                     System.out.println("File renamed successfull !");
                     JLROpener.open(newFileName);
                 } else {
                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf failed to create");
                     System.out.println("File rename operation failed !");
                 }

             } catch (Exception e) {
                 e.printStackTrace();
             }
            
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public void createWord(PrintWordBean printWordBean,String fileLocation,WordFinalPanel wordFinalPanel) {
        String errorMessage = "";
        boolean nextStatus = true;
        String Directory = fileLocation.replaceAll(",", "\\\\");
        String fileName=fileLocation.split(",")[1];
        String processPath = new ProcessManager().getProcessPath();
        try {
            File workingDirectory = new File(processPath);

            File template = new File(workingDirectory.getAbsolutePath() + File.separator + "WordFrmtFirst.tex");
            
            if(!template.exists()) {
                errorMessage += "Processing File";
                nextStatus = false;
            }
            
            File tempDir = null;
            if(nextStatus) {
                tempDir = new File(workingDirectory.getAbsolutePath() + File.separator + "images");
//                if (!tempDir.isDirectory()) {
//                    tempDir.mkdir();
//                }
                if(!tempDir.exists()) {
                    errorMessage += "Images File";
                    nextStatus = false;
                }
            }
            if(nextStatus) {
                File invoice1 = new File(tempDir.getAbsolutePath() + File.separator + "WordFrmtFirst.tex");
         
                JLRConverter converter = new JLRConverter(workingDirectory);
                converter.replace("instituteName", printWordBean.getInstituteName());
                converter.replace("testType", printWordBean.getTestType());
                converter.replace("timePaperTotalText", printWordBean.getTimePaperTotalText());
                converter.replace("mainString", printWordBean.getMainString());

                ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();
		
                ArrayList<String> subservice1 = new ArrayList<String>();
                ArrayList<String> subservice2 = new ArrayList<String>();
                ArrayList<String> subservice3 = new ArrayList<String>();
            
                services.add(subservice1);
                services.add(subservice2);
                services.add(subservice3);		

                converter.replace("services", services);
            
                if (!converter.parse(template, invoice1)) {
                    System.out.println(converter.getErrorMessage());
                }
                services = new ArrayList<ArrayList<String>>();

                subservice1 = new ArrayList<String>();
                subservice2 = new ArrayList<String>();
                subservice3 = new ArrayList<String>();
            
                converter.replace("services", services);
                String command = processPath + "/Word-Pandoc/pandoc.exe" + " -s "+processPath+"/images/WordFrmtFirst.tex -o " + Directory + ".docx";
                try {
                    Process process = Runtime.getRuntime().exec(command);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                File temp = new File(Directory+".docx");
                int k = 0;
                while (!temp.exists() && k < 15) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        /* safe to ignore */ 
                    }
                    k++;
                }
                
                if (!temp.exists()) {
                    JOptionPane.showMessageDialog(wordFinalPanel, fileName + ".docx failed to create");
                } else {
                    JOptionPane.showMessageDialog(wordFinalPanel, fileName + ".docx created successfully");
                    
                    try {
                        JLROpener.open(temp);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if(!nextStatus) {
                JOptionPane.showMessageDialog(wordFinalPanel, "Missing "+errorMessage+" in C Drive.");
            }
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(wordFinalPanel, fileName + ".docx failed to create.");
            System.err.println(ex.getMessage());
        }
    }
    
    public void createOmrSheet(int totalQuestion,int quetionStartNo,String collageName,String fileLocation,PdfFinalPanel pdfFinalPanel,HomePage homePage) {
        String errorMessage = "";
        boolean nextStatus = true;
//        System.out.println("fileLocation: "+fileLocation);
        String Directory=fileLocation.split(",")[0];
        String fileName=fileLocation.split(",")[1];
//        System.out.println("Directory:"+Directory);
//        System.out.println("Filename:"+fileName);
        String omrString = new GenrateOMRLatexCode().getOmrString(totalQuestion, quetionStartNo);
        JFrame frame = null;
        if(pdfFinalPanel != null)
            frame = pdfFinalPanel;
        else if(homePage != null)
            frame = homePage;
        File workingDirectory = new File(new ProcessManager().getProcessPath());
        try {
            File template = new File(workingDirectory.getAbsolutePath() + File.separator + "OMR.tex");
            
            if(!template.exists()) {
                errorMessage += "Processing File";
                nextStatus = false;
            }
            
            File tempDir = null;
            if(nextStatus) {
                tempDir = new File(workingDirectory.getAbsolutePath() + File.separator + "temp");
//                if (!tempDir.isDirectory()) {
//                    tempDir.mkdir();
//                }
                if(!tempDir.exists()) {
                    errorMessage += "Temprary Folder";
                    nextStatus = false;
                }
            }
            
            if(nextStatus) {
                File invoice1 = new File(tempDir.getAbsolutePath() + File.separator + "OMR.tex");
        
                JLRConverter converter = new JLRConverter(workingDirectory);
                converter.replace("omr", omrString);
                converter.replace("college", collageName);

                ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();

                ArrayList<String> subservice1 = new ArrayList<String>();
                ArrayList<String> subservice2 = new ArrayList<String>();
                ArrayList<String> subservice3 = new ArrayList<String>();


                services.add(subservice1);
                services.add(subservice2);
                services.add(subservice3);		

                converter.replace("services", services);	
            
                if (!converter.parse(template, invoice1)) {
                    System.out.println(converter.getErrorMessage());
                }
                
                services = new ArrayList<ArrayList<String>>();
                    
                subservice1 = new ArrayList<String>();
                subservice2 = new ArrayList<String>();
                subservice3 = new ArrayList<String>();
            
                converter.replace("services", services);

                File desktop = new File(Directory);
                JLRGenerator pdfGen = new JLRGenerator();   
                System.out.println("Start");
                if (!pdfGen.generate(invoice1, desktop, workingDirectory)) { 
                    System.out.println(pdfGen.getErrorMessage());
                }
                System.out.println("End");
            
            
                File oldFileName = new File(Directory + "/OMR.pdf");
                File newFileName = new File(Directory + "/" + fileName+"-OMR.pdf");
                try {
                    if (oldFileName.renameTo(newFileName)) {
                        JOptionPane.showMessageDialog(frame, fileName + "-OMR.pdf created successfully");
                        System.out.println("File renamed successfull !");
                        JLROpener.open(newFileName);
                    } else {
                        JOptionPane.showMessageDialog(frame, fileName + "-OMR.pdf failed to create");
                        System.out.println("File rename operation failed !");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(!nextStatus) {
                JOptionPane.showMessageDialog(frame, "Missing "+errorMessage+" in C Drive.");
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public void createFormatTeacherCopyPdf(PrintPdfTeacherCopyBean pdfTeacherCopyBean,String fileLocation) {
        String Directory=fileLocation.split(",")[0];
        String fileName=fileLocation.split(",")[1];
        
        File workingDirectory = new File(new ProcessManager().getProcessPath());

        File template = new File(workingDirectory.getAbsolutePath() + File.separator + "PdfTeacherCopy.tex");

        File tempDir = new File(workingDirectory.getAbsolutePath() + File.separator + "temp");
        if (!tempDir.isDirectory()) {
            tempDir.mkdir();
        }

        File invoice1 = new File(tempDir.getAbsolutePath() + File.separator + "PdfTeacherCopy.tex");

        try {
            JLRConverter converter = new JLRConverter(workingDirectory);
            converter.replace("waterMarkText", pdfTeacherCopyBean.getWaterMarkText());
            converter.replace("footR", pdfTeacherCopyBean.getFooterLeft());
            converter.replace("instituteName", pdfTeacherCopyBean.getInstituteName());
            converter.replace("headerText", pdfTeacherCopyBean.getHeaderText());
            converter.replace("timeDuration", pdfTeacherCopyBean.getTimeDuration());
            converter.replace("totalMarks", pdfTeacherCopyBean.getTotalMarks());
            converter.replace("mainString", pdfTeacherCopyBean.getMainString());
              
            ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();
		
            ArrayList<String> subservice1 = new ArrayList<String>();
            ArrayList<String> subservice2 = new ArrayList<String>();
            ArrayList<String> subservice3 = new ArrayList<String>();
		
            services.add(subservice1);
            services.add(subservice2);
            services.add(subservice3);		
		
            converter.replace("services", services);
            
            if (!converter.parse(template, invoice1)) {
                System.out.println(converter.getErrorMessage());
            }

            services = new ArrayList<ArrayList<String>>();
		
            subservice1 = new ArrayList<String>();
            subservice2 = new ArrayList<String>();
            subservice3 = new ArrayList<String>();
            
            converter.replace("services", services);

            File desktop = new File(Directory);
            JLRGenerator pdfGen = new JLRGenerator();   
            System.out.println("Start");
            if (!pdfGen.generate(invoice1, desktop, workingDirectory)) { 
                System.out.println(pdfGen.getErrorMessage());
            }
            System.out.println("End");
            
            File oldFileName = new File(Directory + "/PdfTeacherCopy.pdf");
            File newFileName = new File(Directory + "/" + fileName+"-TC.pdf");
             try {
                 if (oldFileName.renameTo(newFileName)) {
//                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf created successfully");
                     System.out.println("File renamed successfull !");
                     JLROpener.open(newFileName);
                 } else {
//                     JOptionPane.showMessageDialog(pdfFinalPanel, fileName + ".pdf failed to create");
                     System.out.println("File rename operation failed !");
                 }

             } catch (Exception e) {
                 e.printStackTrace();
             }
            
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}
