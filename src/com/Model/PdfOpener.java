package com.Model;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class PdfOpener {
    public void helpview(String pdfName) {
        File myFile = new File("allPdf/"+pdfName+".pdf");
        try {
            Desktop.getDesktop().open(myFile); 
        } 
        catch (IOException ex) {
            Logger.getLogger(PdfOpener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void openCareer(String name) {
        // build a component controller
        File myFile = new File(name);
        try {
            Desktop.getDesktop().open(myFile); 
        } catch (IOException ex) {
            Logger.getLogger(PdfOpener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void openPdf(String name) {
        File myFile = new File("allPdf/"+name+".pdf");
        // build a component controller
        try {
            Desktop.getDesktop().open(myFile); 
        } catch (IOException ex) {
            Logger.getLogger(PdfOpener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 }