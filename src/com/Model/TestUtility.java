
package com.Model;

import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.bean.ChapterBean;
import com.bean.CountChapterBean;
import com.bean.CountYearBean;
import com.bean.PrintedTestBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewTestBean;
import com.db.operations.ChapterOperation;
import com.db.operations.PrintedTestOperation;
import com.db.operations.SubjectOperation;
import com.id.operations.NewIdOperation;
import com.pages.SingleChapterQuestionsSelection;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Aniket
 */
public class TestUtility {
    
    public boolean saveTest(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<QuestionBean> questionsList,ArrayList<SubjectBean> selectedSubjectList,String selectionType,String testName) {
        boolean returnValue = false;
        int maxTestId = new NewIdOperation().getNewId("PRINTED_TEST_INFO");
        
        PrintedTestBean printedTestBean = new PrintedTestBean();
        printedTestBean.setPrintedTestId(maxTestId);//1
        printedTestBean.setTestName(testName);//2
        String quesId = ",";
        
        for(QuestionBean questionsBean : selectedQuestionsList)
            quesId += questionsBean.getQuestionId()+",";
        
        printedTestBean.setQuesIds(quesId);//3
        printedTestBean.setTotalQues(questionsList.size());//4
        printedTestBean.setQuesType(selectionType);//5
        String subjectIds = "";
        
        for(SubjectBean subjectBean : selectedSubjectList)
            subjectIds += subjectBean.getSubjectId()+",";
            
        printedTestBean.setSubjectIds(subjectIds);//6
        printedTestBean.setTestDateTime(new Timestamp(System.currentTimeMillis()));//7
        
        ObjectOutputStream objectOutputStream = null;
        
        try {
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(arrayOutputStream);
            objectOutputStream.writeObject(questionsList);
            objectOutputStream.flush();
            objectOutputStream.close();
            arrayOutputStream.close();
            printedTestBean.setQuestionListObject(arrayOutputStream.toByteArray());//8
            returnValue = true;
        } catch (IOException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            try {
                objectOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        if(returnValue) {
            if (new PrintedTestOperation().insertPrintedTest(printedTestBean))
                returnValue = true;
            else
                returnValue = false;
        }
        return returnValue;
    }
    
    public ViewTestBean saveTests(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<QuestionBean> questionsList,ArrayList<SubjectBean> selectedSubjectList,String selectionType,String testName) {
        boolean statusValue = false;
        ViewTestBean returnBean = null;
        int maxTestId = new NewIdOperation().getNewId("PRINTED_TEST_INFO");
        PrintedTestBean printedTestBean = new PrintedTestBean();
        printedTestBean.setPrintedTestId(maxTestId);//1
        printedTestBean.setTestName(testName);//2
        String quesId = ",";
        
        for(QuestionBean questionsBean : selectedQuestionsList)
            quesId += questionsBean.getQuestionId()+",";
        
        printedTestBean.setQuesIds(quesId);//3
        printedTestBean.setTotalQues(questionsList.size());//4
        printedTestBean.setQuesType(selectionType);//5
        String subjectIds = "";
        
        for(SubjectBean subjectBean : selectedSubjectList)
            subjectIds += subjectBean.getSubjectId()+",";
            
        printedTestBean.setSubjectIds(subjectIds);//6
        printedTestBean.setTestDateTime(new Timestamp(System.currentTimeMillis()));//7
        
        ObjectOutputStream objectOutputStream = null;
        
        try {
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(arrayOutputStream);
            objectOutputStream.writeObject(questionsList);
            objectOutputStream.flush();
            objectOutputStream.close();
            arrayOutputStream.close();
            printedTestBean.setQuestionListObject(arrayOutputStream.toByteArray());//8
            statusValue = true;
        } catch (IOException ex) {
            statusValue = false;
            ex.printStackTrace();
        } finally {
            try {
                objectOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        if(statusValue) {
            if (new PrintedTestOperation().insertPrintedTest(printedTestBean)) {
                returnBean = new ViewTestBean();
                returnBean.setTitleName(selectionType);
                returnBean.setSelectedSubjects(getSelectedSubjects(selectedSubjectList, printedTestBean.getSubjectIds()));
                returnBean.setSaveTime(getStringDateTime(printedTestBean.getTestDateTime()));
                returnBean.setTotalQuestions(printedTestBean.getTotalQues());
                returnBean.setPrintedTestBean(printedTestBean);
            } else {
                returnBean = null;
            }
        }
        return returnBean;
    }
    
    public void resumeTest(PrintedTestBean printedTestBean) {
        String[] tempQuesIds = printedTestBean.getQuesIds().split(",");
        ArrayList<QuestionBean> questionsList = (ArrayList<QuestionBean>)getTestBeanObject(printedTestBean);
        ArrayList<QuestionBean> selectedQuestionsList = new ArrayList<QuestionBean>();
        if(printedTestBean.getQuesType().trim().equalsIgnoreCase("YearWise")) {
            ArrayList<String> yearList = new ArrayList<String>();
            for(QuestionBean bean : questionsList) {
                for(int i=1; i<tempQuesIds.length; i++) {
                    int quesId = Integer.parseInt(tempQuesIds[i]);
                    if(quesId == bean.getQuestionId()) {
                        bean.setSelected(true);
                        selectedQuestionsList.add(bean);
                    }
                }
                
                if(!yearList.contains(bean.getYear().trim())) {
                    yearList.add(bean.getYear());
                }
            }
            ArrayList<CountYearBean> selectedYearCountList = null;
            CountYearBean countYearBean = null;
            
            for (String year : yearList) {
                int totalQues = 0;
            
                for(QuestionBean questionsBean : questionsList) {
                    if (questionsBean.getYear().trim().equals(year.trim()))
                        totalQues += 1;
                }
            
                countYearBean = new CountYearBean();
                countYearBean.setYear(year);
                countYearBean.setTotalQuestions(totalQues);
            
                if (selectedYearCountList == null) 
                    selectedYearCountList = new ArrayList<CountYearBean>();
            
                    selectedYearCountList.add(countYearBean);
            }
            new MultipleYearQuestionsSelection(selectedYearCountList, questionsList,selectedQuestionsList,printedTestBean.getQuesType(),printedTestBean).setVisible(true);
        } else if(printedTestBean.getQuesType().trim().equalsIgnoreCase("ChapterWise")) {
            ChapterBean selectedChapterBean = new ChapterOperation().getChapterBean(questionsList.get(0).getChapterId());
            SubjectBean selectedSubjectBean = new SubjectOperation().getSubjectBean(questionsList.get(0).getSubjectId());
            
            for(QuestionBean bean : questionsList) {
                for(int i=1;i<tempQuesIds.length;i++) {
                    int quesId = Integer.parseInt(tempQuesIds[i]);
                    if(quesId == bean.getQuestionId()) {
                        bean.setSelected(true);
                        selectedQuestionsList.add(bean);
                    }
                }
            }
            new SingleChapterQuestionsSelection(selectedChapterBean, selectedSubjectBean, questionsList, selectedQuestionsList,printedTestBean.getQuesType().trim(),printedTestBean).setVisible(true);
        } else {
            ArrayList<Integer> chapterIdList = new ArrayList<Integer>();

            for(QuestionBean bean : questionsList) {
                for(int i=1;i<tempQuesIds.length;i++) {
                    int quesId = Integer.parseInt(tempQuesIds[i]);
                    if(quesId == bean.getQuestionId()) {
                        bean.setSelected(true);
                        selectedQuestionsList.add(bean);
                    }
                }

                if(!chapterIdList.contains(bean.getChapterId())) {
                    chapterIdList.add(bean.getChapterId());
                }
            }
            Collections.sort(chapterIdList);
            ArrayList<ChapterBean> chapterList = new ChapterOperation().getChaptersList(chapterIdList);
            ArrayList<SubjectBean> subjectList = new SubjectOperation().getSelectedSubjectsList(chapterList);
            
            ArrayList <CountChapterBean> countedChapterList = null;
            CountChapterBean ccBean = null;
            for(ChapterBean bean : chapterList) {
                int totalQuestions =0;

                for(QuestionBean questionsBean : questionsList) {
                    if(bean.getChapterId() == questionsBean.getChapterId()) {
                        totalQuestions++;
                    }
                }

                if(countedChapterList == null)
                    countedChapterList = new ArrayList<CountChapterBean>();

                ccBean = new CountChapterBean();
                ccBean.setChapterBean(bean);
                ccBean.setTotalQuestions(totalQuestions);
                countedChapterList.add(ccBean);
            }
            new MultipleChapterQuestionsSelection(questionsList, chapterList, subjectList, countedChapterList, selectedQuestionsList,printedTestBean.getQuesType(),printedTestBean).setVisible(true);
        }
    }
    
    private String getStringDateTime(Timestamp timestamp) {
        String returnDate = new SimpleDateFormat("dd-MM-yy hh:mm a").format(timestamp.getTime());
        return returnDate;
    }
    
    private String getSelectedSubjects(ArrayList<SubjectBean> subjectList,String subjectIds) {
        String returnString = "";
        String[] subjectIdArray = subjectIds.split(",");
        
        int subId = 0;
        if(subjectIdArray.length == 1) {
            returnString = "Subject : ";
            subId = Integer.parseInt(subjectIdArray[0]);
            for(SubjectBean subjectBean : subjectList) {
                if(subjectBean.getSubjectId() == subId) {
                    returnString += subjectBean.getSubjectName();
                    break;
                }
            }
        } else {
            returnString = "Group : ";
            for(String subjectId : subjectIdArray) {
                subId = Integer.parseInt(subjectId);
                for(SubjectBean subjectBean : subjectList) {
                    if(subjectBean.getSubjectId() == subId) {
                        returnString += subjectBean.getSubjectName().substring(0, 1);
                        break;
                    }
                }
            }
        }
        return returnString;
    }
    
    private Object getTestBeanObject (PrintedTestBean printedTestBean) {
        Object object = null;
        try {
            byte[] data = printedTestBean.getQuestionListObject();
            ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
            ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
            object = objInStream.readObject();
            objInStream.close();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        
        return object;
    }
}
