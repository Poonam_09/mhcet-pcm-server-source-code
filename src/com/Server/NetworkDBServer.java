/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Server;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import ui.ConfigureServer;

public class NetworkDBServer {
        private static int NETWORKSERVER_PORT1=1621;
        private static int NETWORKSERVER_PORT2=1622;
        public static final String DERBY_CLIENT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
	private static final String DERBY_CLIENT_URL= "jdbc:derby://localhost:"+NETWORKSERVER_PORT1+"/MERGEDB;create=true;root;root";
	
//        public static final String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";	
//	public static final String DERBY_EMBEDDED_URL="jdbc:derby:NSSampledb;";
	        
        NetworkServerUtil nwServer1;    
        NetworkServerUtil nwServer2;    
        PrintWriter pw = null;
        
        public boolean start1(){
            pw = new PrintWriter(System.out,true);	// to print messages  
            String serverIP=Server.getServerIP();
            if(!serverIP.equals("")){
            pw.println("Using JDBC driver: " + DERBY_CLIENT_DRIVER);
            {
                nwServer1 = new NetworkServerUtil(serverIP,NETWORKSERVER_PORT1,pw);
		nwServer1.start();

		boolean knowIfServerUp = false; //do we know if server is ready to accept connections
		int numTimes = 5;

		// Test to see if server is ready for connections, for 15 seconds.
		while(!knowIfServerUp && (numTimes >0)) {
                    try {
			// testing for connection to see if the network server is up and running
			// if server is not ready yet, this method will throw an exception
			numTimes--;
			nwServer1.testForConnection();
			knowIfServerUp = true;
                    }
                    catch(Exception e) {
			System.out.println("[NsSample] Unable to obtain a connection to network server, trying again after 3000 ms.");
                        try {
                            Thread.currentThread().sleep(3000);
                        } 
                        catch (InterruptedException ex) {
//                            Logger.getLogger(NetworkDBServer.class.getName()).log(Level.SEVERE, null, ex);
                            new ConfigureServer().setVisible(true);
                        }
                    }
		}
		if(!knowIfServerUp) {
                        pw.println("[NsSample] Exiting, since unable to connect to Derby Network Server.");
			pw.println("[NsSample] Please try to increase the amount of time to keep trying to connect to the Server.");
//			System.exit(1);
                        return false;
		}
		pw.println("[NsSample] Derby Network Server started.");
                return true;
            }
            }
            else{
                Object[] options = { "YES", "CANCEL" };
                int i = JOptionPane.showOptionDialog(null, "Unable to Start Database Server. Please check Server IP Address", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
                if(i==0)
                {
                    new ConfigureServer().setVisible(true);
                }
            }
            return false;
        }
        
        public void start2(){
            pw = new PrintWriter(System.out,true);	// to print messages  
            String serverIP=Server.getServerIP();
            if(!serverIP.equals("")){
            pw.println("Using JDBC driver: " + DERBY_CLIENT_DRIVER);
            {
                nwServer2 = new NetworkServerUtil(serverIP,NETWORKSERVER_PORT2,pw);
		nwServer2.start();

		boolean knowIfServerUp = false; //do we know if server is ready to accept connections
		int numTimes = 5;

		// Test to see if server is ready for connections, for 15 seconds.
		while(!knowIfServerUp && (numTimes >0)) {
                    try {
			// testing for connection to see if the network server is up and running
			// if server is not ready yet, this method will throw an exception
			numTimes--;
			nwServer2.testForConnection();
			knowIfServerUp = true;
                    }
                    catch(Exception e) {
			System.out.println("[NsSample] Unable to obtain a connection to network server, trying again after 3000 ms.");
                        try {
                            Thread.currentThread().sleep(3000);
                        } 
                        catch (InterruptedException ex) {
                            Logger.getLogger(NetworkDBServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
		}
		if(!knowIfServerUp) {
                        pw.println("[NsSample] Exiting, since unable to connect to Derby Network Server.");
			pw.println("[NsSample] Please try to increase the amount of time to keep trying to connect to the Server.");
			System.exit(1);
		}
		pw.println("[NsSample] Derby Network Server started.");
            }
            }
            else{
//                Object[] options = { "YES", "CANCEL" };
//                int i = JOptionPane.showOptionDialog(null, "Unable to Start Database Server. Please check Server IP Address", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
//                if(i==0)
//                {
//                    new ConfigureServer().setVisible(true);
//                }
            }
        }
        
        
        public Connection getClientConnection(String serverIP){
            // Load the JDBC Driver
            int NETWORKSERVER_PORT=1621;    
            String databaseName = "MERGEDB;";
            String DERBY_CLIENT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
            String DERBY_CLIENT_URL= "jdbc:derby://"+serverIP+":"+NETWORKSERVER_PORT+"/"+databaseName;
		try{
                    Class.forName(DERBY_CLIENT_DRIVER).newInstance();
		} 
                catch (Exception e) {
                    System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
                    e.printStackTrace();
                    System.exit(1);  //critical error, so exit
		}
		
		Properties properties = new java.util.Properties();		
		properties.setProperty("user","root");
		properties.setProperty("password","root");

		// Get database connection via DriverManager api
		try{			
//                    Connection conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL, properties);
                    Connection conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL,"root","root");
                    System.out.println("Connection request Successful ");
                    return conn;
		} 
                catch(Exception e){
			System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
			System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
			e.printStackTrace();
			System.exit(1);  //critical error, so exit
		  }

		return null;
        }
        
        public Connection getEmbeddedConnection(){
                // Load the JDBC Driver
                String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.ClientDriver";
                String DERBY_EMBEDDED_URL="jdbc:derby:MERGEDB;";
		try{
                    Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
		} 
                catch (Exception e) {
                    System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
                    e.printStackTrace();
                    System.exit(1);  //critical error, so exit
		}
		
		Properties properties = new java.util.Properties();		
		properties.setProperty("user","root");
		properties.setProperty("password","root");

		// Get database connection via DriverManager api
		try{			
                    Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL,"root","root");
                    System.out.println("Embedded Connection request Successful ");
                    return conn;
		} 
                catch(Exception e){
			System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
			System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
			e.printStackTrace();
			System.exit(1);  //critical error, so exit
		  }

		return null;
        }
                
        public void shutDown(){
                 System.out.println(" Shutting down network server.");
		 nwServer1.shutdown();
                 nwServer2.shutdown();
		 System.out.println(" End of Network server.");
        }
        
        public static void main(String[] s){
                    NetworkDBServer networkDBServer = new NetworkDBServer();
                    networkDBServer.start1();
                    networkDBServer.getClientConnection("localhost");
                    networkDBServer.getEmbeddedConnection();
                    networkDBServer.shutDown();
        }
}
