/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Word.Generation;

import com.Model.FileChooser;
import com.Model.PrintWord;
import com.Model.TestUtility;
import com.bean.PdfSecondFormatBean;
import com.bean.PrintedTestBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewTestBean;
import com.bean.WordPageSetupBean;
import com.db.operations.PrintedTestOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.QuestionPaperOperation;
import com.pages.HomePage;
import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.pages.SingleChapterQuestionsSelection;
import com.Model.TitleInfo;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class WordFinalPanel extends javax.swing.JFrame {

    private WordPageSetupBean wordPageSetupBean;
    private PdfSecondFormatBean pdfSecondFormatBean;
    private WordHeadingSetup wordHeadingSetup;
    private String printingPaperType;
    private ArrayList<SubjectBean> selectedSubjectList;
    private ArrayList<QuestionBean> selectedQuestionList;
    private ArrayList<QuestionBean> questionList;
    private Object quesPageObject;
    private WordPageSetup wordPageSetup;

    public WordFinalPanel() {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
    }

    public WordFinalPanel(WordPageSetupBean wordPageSetupBean, PdfSecondFormatBean pdfSecondFormatBean,ArrayList<QuestionBean> selectedQuestionList,ArrayList<SubjectBean> selectedSubjectList,String printingPaperType,int totalMarks,ArrayList<QuestionBean> questionList,WordHeadingSetup wordHeadingSetup,Object quesPageObject,WordPageSetup wordPageSetup) {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.wordPageSetupBean = wordPageSetupBean;
        this.pdfSecondFormatBean = pdfSecondFormatBean;
        this.wordHeadingSetup = wordHeadingSetup;
        this.printingPaperType = printingPaperType;
        this.selectedQuestionList = selectedQuestionList;
        this.selectedSubjectList = selectedSubjectList;
        this.questionList = questionList;
        this.quesPageObject = quesPageObject;
        this.wordPageSetup = wordPageSetup;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        MainPanel = new javax.swing.JPanel();
        BodyPanel = new javax.swing.JPanel();
        ChkTwoCloumnCompatible = new javax.swing.JCheckBox();
        ChkAnswerSheet = new javax.swing.JCheckBox();
        ChkSolutionSheet = new javax.swing.JCheckBox();
        ChkSaveTest = new javax.swing.JCheckBox();
        FooterPanel = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnCreate = new javax.swing.JButton();

        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jPanel3.setName("jPanel3"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 255));
        setFocusCycleRoot(false);
        setForeground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        MainPanel.setBackground(new java.awt.Color(255, 255, 255));
        MainPanel.setName("MainPanel"); // NOI18N

        BodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        BodyPanel.setName("BodyPanel"); // NOI18N

        ChkTwoCloumnCompatible.setBackground(new java.awt.Color(11, 45, 55));
        ChkTwoCloumnCompatible.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkTwoCloumnCompatible.setForeground(new java.awt.Color(255, 255, 255));
        ChkTwoCloumnCompatible.setText("Two Column Compatible");
        ChkTwoCloumnCompatible.setName("ChkTwoCloumnCompatible"); // NOI18N

        ChkAnswerSheet.setBackground(new java.awt.Color(11, 45, 55));
        ChkAnswerSheet.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkAnswerSheet.setForeground(new java.awt.Color(255, 255, 255));
        ChkAnswerSheet.setSelected(true);
        ChkAnswerSheet.setText("Create Correct Answer Sheet");
        ChkAnswerSheet.setName("ChkAnswerSheet"); // NOI18N

        ChkSolutionSheet.setBackground(new java.awt.Color(11, 45, 55));
        ChkSolutionSheet.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSolutionSheet.setForeground(new java.awt.Color(255, 255, 255));
        ChkSolutionSheet.setSelected(true);
        ChkSolutionSheet.setText("Create Solution Sheet");
        ChkSolutionSheet.setName("ChkSolutionSheet"); // NOI18N

        ChkSaveTest.setBackground(new java.awt.Color(11, 45, 55));
        ChkSaveTest.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSaveTest.setForeground(new java.awt.Color(255, 255, 255));
        ChkSaveTest.setText("Save Test");
        ChkSaveTest.setName("ChkSaveTest"); // NOI18N

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BodyPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ChkSaveTest)
                    .addComponent(ChkSolutionSheet)
                    .addComponent(ChkAnswerSheet, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkTwoCloumnCompatible))
                .addGap(47, 47, 47))
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BodyPanelLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(ChkTwoCloumnCompatible)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkAnswerSheet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkSolutionSheet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkSaveTest)
                .addGap(22, 22, 22))
        );

        FooterPanel.setBackground(new java.awt.Color(255, 255, 255));
        FooterPanel.setName("FooterPanel"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(208, 87, 96));
        BtnBack.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setBorderPainted(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BtnCreate.setBackground(new java.awt.Color(208, 87, 96));
        BtnCreate.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnCreate.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreate.setText("Create");
        BtnCreate.setBorderPainted(false);
        BtnCreate.setName("BtnCreate"); // NOI18N
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FooterPanelLayout = new javax.swing.GroupLayout(FooterPanel);
        FooterPanel.setLayout(FooterPanelLayout);
        FooterPanelLayout.setHorizontalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BtnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        FooterPanelLayout.setVerticalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FooterPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
        // TODO add your handling code here:
        String fileLocation = new FileChooser().getWordFileLocation(this);
        if(fileLocation != null) {
            if(ChkSaveTest.isSelected()) {
                String testName = JOptionPane.showInputDialog(rootPane, "Enter Test Name:","Saved Test", JOptionPane.QUESTION_MESSAGE);
                if(testName != null) {
                    if(testName.trim().isEmpty())
                        testName = "Test";
                    SaveTest(testName);
                }
            }
            new PrintWord().setPrintWord(wordPageSetupBean, pdfSecondFormatBean, selectedSubjectList, selectedQuestionList, printingPaperType, ChkTwoCloumnCompatible.isSelected(), ChkAnswerSheet.isSelected(), ChkSolutionSheet.isSelected(), this, fileLocation);
            wordHeadingSetup.dispose();
            wordPageSetup.dispose();
            
            if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(HomePage.class.isInstance(quesPageObject)) {
                HomePage frm = (HomePage)quesPageObject;
                frm.setEnabled(true);
            }
            setAttemptedValue();
            this.dispose();
        }
    }//GEN-LAST:event_BtnCreateActionPerformed

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        // TODO add your handling code here:
        if (wordHeadingSetup != null) {
            wordHeadingSetup.setVisible(true);
        }
        this.dispose();
    }//GEN-LAST:event_BtnBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(WordFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(WordFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(WordFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(WordFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        /* Create and display the form */
        new WordFinalPanel().setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JCheckBox ChkAnswerSheet;
    private javax.swing.JCheckBox ChkSaveTest;
    private javax.swing.JCheckBox ChkSolutionSheet;
    private javax.swing.JCheckBox ChkTwoCloumnCompatible;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
    
    private void SaveTest(String testName) {
        boolean testSaveStatus = false;
        ArrayList<Integer> saveTestSelectedQuesIdList = null;
        PrintedTestBean printedTestBean = null;
        SingleChapterQuestionsSelection singleQuesPanel = null;
        MultipleChapterQuestionsSelection multipleQuesPanel = null;
        MultipleYearQuestionsSelection multiplePaperQuesPanel = null;
        HomePage homePage = null;
        
        if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            singleQuesPanel = (SingleChapterQuestionsSelection)quesPageObject;
            testSaveStatus = singleQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = singleQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = singleQuesPanel.getPrintedTestBean();
        } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            multipleQuesPanel = (MultipleChapterQuestionsSelection)quesPageObject;
            testSaveStatus = multipleQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = multipleQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = multipleQuesPanel.getPrintedTestBean();
        } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
            multiplePaperQuesPanel = (MultipleYearQuestionsSelection)quesPageObject;
            testSaveStatus = multiplePaperQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = multiplePaperQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = multiplePaperQuesPanel.getPrintedTestBean();
        } else if(HomePage.class.isInstance(quesPageObject)) {
            homePage = (HomePage)quesPageObject;
            testSaveStatus = false;
            saveTestSelectedQuesIdList = null;
            printedTestBean = null;
        }
        
        if(testSaveStatus && saveTestSelectedQuesIdList != null) {
            ArrayList<Integer> selectedQuesIdList = new ArrayList<Integer>();
            for(QuestionBean bean : selectedQuestionList) {
                if(!saveTestSelectedQuesIdList.contains(bean.getQuestionId())) {
                    testSaveStatus = false;
                    break;
                }
                selectedQuesIdList.add(bean.getQuestionId());
            }
            
            if(saveTestSelectedQuesIdList.size() != selectedQuesIdList.size())
                testSaveStatus = false;
            
            if(testSaveStatus) {
                for(int quesId : saveTestSelectedQuesIdList) {
                    if(!selectedQuesIdList.contains(quesId)) {
                        testSaveStatus = false;
                        break;
                    }
                }
            }
            
            if(!testSaveStatus) {
                new PrintedTestOperation().deletePrintedTest(printedTestBean);
            }
        }
        
        if(!testSaveStatus) {
            ViewTestBean viewTestBean = new TestUtility().saveTests(selectedQuestionList,questionList,selectedSubjectList, printingPaperType, testName);
            testSaveStatus = true;
            printedTestBean = viewTestBean.getPrintedTestBean();
            
            if(saveTestSelectedQuesIdList == null)
                saveTestSelectedQuesIdList = new ArrayList<Integer>();
            else
                saveTestSelectedQuesIdList.clear();
            for(QuestionBean bean : selectedQuestionList) 
                saveTestSelectedQuesIdList.add(bean.getQuestionId());
            
            if(singleQuesPanel != null) {
                singleQuesPanel.setTestSaveStatus(testSaveStatus);
                singleQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                singleQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(multipleQuesPanel != null) {
                multipleQuesPanel.setTestSaveStatus(testSaveStatus);
                multipleQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                multipleQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(multiplePaperQuesPanel != null) {
                multiplePaperQuesPanel.setTestSaveStatus(testSaveStatus);
                multiplePaperQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                multiplePaperQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(homePage != null) {
                ArrayList<PrintedTestBean> testList = homePage.getTestList();
                if(testList == null)
                    testList = new ArrayList<PrintedTestBean>();
                testList.add(printedTestBean);
                Collections.sort(testList);
                homePage.setTestList(testList);
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Your Test Not Saved.\nYou Have Already Same Saved Test");
        }
    }
    
    private void setAttemptedValue() {
        ArrayList<QuestionBean> updateQuesList = null;
        for(QuestionBean questionBean : selectedQuestionList) {
            if(questionBean.getAttempt() == 0) {
                if(updateQuesList == null)
                    updateQuesList = new ArrayList<QuestionBean>();
                questionBean.setAttempt(1);
                updateQuesList.add(questionBean);
            }
        }

        if(updateQuesList != null && printingPaperType.equalsIgnoreCase("YearWise"))
            new QuestionPaperOperation().updateUsedValue(updateQuesList);
        else if(updateQuesList != null)
            new QuestionOperation().updateUsedValue(updateQuesList);
    }
}
