/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

import com.bean.QuestionBean;
import com.db.operations.QuestionOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class AnswerSheetProcessing {
    
    public String getAnswerSheet(ArrayList <QuestionBean> selectedQuestionList) {
        String completeAnswerSheeet = "";
        String column = "cc|";
        String start = "\\begin{longtable}{|";
        String end = "\\end{longtable}";
        int index = 1;
        
        if(selectedQuestionList.size() < 100){
            for(int i=0; i<12; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 12 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 12 != 0){
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 12;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else if(selectedQuestionList.size() < 1000) {
            for(int i=0; i<11; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 11 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 11 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 11;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else {
            for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 10 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 11;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        }
//        System.out.println(completeAnswerSheeet);
        return completeAnswerSheeet;
    }
    
    public String getAnswerSheet(ArrayList <QuestionBean> selectedQuestionList,int startNo) {
        String completeAnswerSheeet = "";
        String column = "cc|";
        String start = "\\begin{longtable}{|";
        String end = "\\end{longtable}";
        int index = 1;
        int flag = 0;
        if(selectedQuestionList.size() < 100 && startNo < 100) 
            flag = 1;
        else if(selectedQuestionList.size() < 100 && startNo < 1000) 
            flag = 2;
        else if(selectedQuestionList.size() < 100 && startNo >= 1000) 
            flag = 3;
        else if(selectedQuestionList.size() < 1000 && startNo < 1000)
            flag = 2;
        else 
            flag = 3;
        if(flag == 1){
            for(int i=0; i<12; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                if(index % 12 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 12 != 0){
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 12;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else if(flag == 2) {
            for(int i=0; i<11; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                if(index % 11 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 11 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 11;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else {
            for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                if(index % 10 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 10;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        }
//        System.out.println(completeAnswerSheeet);
        return completeAnswerSheeet;
    }
    
    public static void main(String[] args) {
        ArrayList<QuestionBean> quesList = new QuestionOperation().getQuestuionsChapterWise(27);
//        for(int i=1001;i<=86;i++)
//            quesList
        
        System.out.println("QUUS:" + new AnswerSheetProcessing().getAnswerSheet(quesList, 1001));
    }
    
}
